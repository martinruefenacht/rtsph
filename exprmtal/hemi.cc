#include <omp.h>

#include "BVH.h"
#include "Kernel.h"
#include "Parser.h"
#include "Image.h"

#define sx 500
#define sy 500

int main(int argc, char** argv) {
    /*double start, end;

    std::clog << "parsing" << std::endl;
    Spheres sphs;
    start = omp_get_wtime();
    parse("aux/data/Data_025", sphs);
    end = omp_get_wtime();
    std::clog << "parse " << (end-start) << std::endl;

    //generate tree
    std::clog << "building" << std::endl;
    BVH tree;
    start = omp_get_wtime();
    mkSAHBVH(sphs, tree);
    end = omp_get_wtime();
    std::clog << "build " << (end-start) << std::endl;
    sphs.deallocate();

    //computation
    std::clog << "calculating" << std::endl;
    Image image(sx, sy);

    double st = omp_get_wtime();

    Vector3 min(0, 0, 0);
    Vector3 max(10000, 10000, 10000);
    
    Vector3 d = max; d -= min;

    leafs_t leafs;
    leafs.reserve(500);

    for(uint64_t iy = 0; iy < sy; ++iy) {
        if(iy % 50 == 0)
            std::cout << "state " << iy << std::endl;

        for(uint64_t ix = 0; ix < sx; ++ix) {
            //generate ray
            Ray ray; 
            ray.origin = Vector3(5000);
            ray.tmin = 0.0;
            ray.tmax = 15000.0;

            double theta = M_PI*iy/(double)sy;
            double phi = 2*M_PI*ix/(double)sx;

            double dx, dy, dz;
            dx = sin(theta)*cos(phi);
            dy = sin(theta)*sin(phi);
            dz = cos(theta);
            
            ray.direction = Vector3(dx, dy, dz);
            ray.direction.normalize();

            //broadphase
            leafs.clear();
            tree.traverse(ray, leafs);

            //narrowphase
            real_t cdensity = 0.0;
            for(uint32_t ileaf = 0; ileaf < leafs.size(); ++ileaf) {
                BVHLeaf lf = leafs[ileaf];

                //iterate spheres in leaf
                for(uint32_t inc = 0; inc < lf.run; ++inc) {
                    uint32_t idx = lf.offset + inc;
                    
                    //intersect
                    real_t t0, t1;
                    if(!tree.spheres.intersect(idx, ray, t0, t1)) 
                        continue;

                    //filter
                    //precision problems
                    if((t0 == tree.spheres.r[idx]) || 
                       (t1 == tree.spheres.r[idx])) {
                        printf("flag\n");
                        continue; //XXX flag
                    }
                    else if(-t0 > t1) {
                        //printf("sph skipped\n");
                        continue;
                    }

                    //calculate normalized impact parameter
                    //TODO optimize, 17 
                    real_t tp = (t0 + t1) * 0.5;

                    //Vector3 bv;
                    //ray(tp, bv);

                    double bx, by, bz;
                    bx = tp * ray.direction.com[0];
                    by = tp * ray.direction.com[1];
                    bz = tp * ray.direction.com[2];
                    
                    bx += (ray.origin.com[0] - tree.spheres.cx[idx]);
                    by += (ray.origin.com[1] - tree.spheres.cy[idx]);
                    bz += (ray.origin.com[2] - tree.spheres.cz[idx]);

                    double len2 = (bx*bx) + (by*by) + (bz*bz);
                    double len = sqrt(len2);
                    double nb = len * tree.spheres.ir[idx];

                    //real_t nb = bv.length() * tree.spheres.ir[idx];
                    //real_t nb = bv.length2() * tree.spheres.ir2[idx];
                    
                    //calculate weight
                    double kern = kernel(nb) * 8.0;
                    cdensity += (kern * tree.spheres.ir2[idx]);
                }
            }

            image.setValue(ix, iy, log(cdensity));
        }
    }
    double en = omp_get_wtime();

    std::clog << "outputing" << std::endl;
    image.write("hemi.pnm");
    printf("TT  %0.6f\n", (en-st));
    printf("TPR %0.6f\n", ((en-st)/((double)sx*(double)sy)));*/
}

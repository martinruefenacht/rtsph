#pragma once

#include <vector>

#include "Common.h"
#include "Tree.h"
#include "AABB.h"

struct SBVHNode {
    //bounding box
    SmitAABB bbox;
    
    //internal node
    uint32_t left, right;

    //leaf node
    uint32_t off, run;
    bool leaf;
};

typedef SBVHNode sbnode_t;
typedef const sbnode_t sbnode_ct;
typedef std::vector<sbnode_t> sbnodes_t;

class SBVH { //: public Tree {
public:
    void traverse(ray_ct&, sphints_t&) const;
    void print() const;

    //sphere references
    spheres_ct* sphs;
    sphrefs_t refs;

    //nodes
    sbnodes_t nodes;

private:
    void broadphase(ray_ct&, sphrefs_t&) const;
};

//BVH Factories
void mkSBVH(spheres_ct&, SBVH&);

#include "Brute.h"

#include <cassert>
#include <algorithm>

void mkBrute(spheres_ct& spheres, Brute& tree) {
    assert(spheres.size() > 0);

    //Naive does nothing...
    tree.sphs = &spheres;
}

void Brute::traverse(ray_ct& ray, sphints_t& intersects) const {
    //narrow phase for all spheres
    for(sphref_t ref = 0; ref < sphs->size(); ++ref) {
        sphere_ct& sph = (*sphs)[ref];

        real_t t0, t1;
        if(sph.intersect(ray, t0, t1)) {
            sphint_t intersect;
            intersect.ref = ref;
            intersect.t0 = t0;
            intersect.t1 = t1;
            intersects.push_back(intersect);
        }
    }
}

void Brute::print() const {
    return ;
}

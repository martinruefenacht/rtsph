#include <string>
#include <cassert>
#include <cmath>
#include <sstream>

#include <mpi.h>

#include "BVH.h"
#include "Kernel.h"
#include "Parser.h"
#include "Image.h"
#include "Worker.h"

#define sx 800
#define sy 100

int main(int argc, char** argv) {
    //MPI setup
    /*MPI_Init(&argc, &argv);

    int ranks, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &ranks);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    //double start = MPI_Wtime();
    Spheres sphs;
    parse("aux/data/Data_025", sphs);
    //double end = MPI_Wtime();

    //printf("rank %i time %f\n", rank, (end-start));

    //generate tree
    BVH tree;
    mkSAHBVH(sphs, tree);

    //computation
    //Image image(sx, sy);
    //Image image(sx, sy/ranks);
    int si = sx * (sy/ranks);
    double *image = new double[sx*(sy/ranks)];

    uint32_t yoff = sy/ranks*rank;

    //std::cout << "calculating" << std::endl;
    //double st = omp_get_wtime();

    Vector3 min(0,0,0);
    Vector3 max(10000,10000,10000);
    Vector3 dir(0.0, 0.0, 1.0);
    
    Vector3 d = max; d -= min;

    double4 dx, dy;
    dx = _mm256_set1_pd(d.com[0]);
    dy = _mm256_set1_pd(d.com[1]);

    leafs_t leafs;
    leafs.reserve(1000);

    for(uint64_t iy = 0; iy < sy/(uint32_t)ranks; iy += 2) {
        for(uint64_t ix = 0; ix < sx; ix += 2) {
            double4 cdensity = _mm256_setzero_pd();

            //generate ray packet
            double pxh = (ix+0.5)/(double)sx;
            double pyh = (iy+yoff+0.5)/(double)sy;
            double pxf = (ix+1.5)/(double)sx;
            double pyf = (iy+yoff+1.5)/(double)sy;

            RPacket packet;
            //packet origin
            packet.ox = _mm256_set1_pd(min.com[0]);
            packet.oy = _mm256_set1_pd(min.com[1]);
            packet.oz = _mm256_set1_pd(min.com[2]);
            __m256d ptx = _mm256_setr_pd(pxh, pxf, pxh, pxf);
            __m256d pty = _mm256_setr_pd(pyh, pyh, pyf, pyf);
            __m256d offx = _mm256_mul_pd(dx, ptx);
            __m256d offy = _mm256_mul_pd(dy, pty);
            packet.ox = _mm256_add_pd(packet.ox, offx);
            packet.oy = _mm256_add_pd(packet.oy, offy);
            
            //packet directions
            packet.dx = _mm256_set1_pd(0.0);
            packet.dy = _mm256_set1_pd(0.0);
            packet.dz = _mm256_set1_pd(1.0);

            //packet length
            packet.tmin = _mm256_set1_pd(0.0);
            packet.tmax = _mm256_set1_pd(15000.0);

            //broadphase
            leafs.clear();
            tree.traverse(packet, leafs);

            //narrowphase
            for(uint32_t ileaf = 0; ileaf < leafs.size(); ++ileaf) {
                BVHLeaf lf = leafs[ileaf];

                //iterate spheres in leaf
                for(uint32_t inc = 0; inc < lf.run; ++inc) {
                    uint32_t idx = lf.offset + inc;
                    //double tmp[4];

                    //intersect sphere
                    double4 active, t0, t1;
                    tree.spheres.intersect(idx, packet, active, t0, t1);

                    //t0 = _mm256_and_pd(t0, active);
                    //t1 = _mm256_and_pd(t1, active);

                    //filter
                    //TODO requires special flag filter
                    double4 nt0 = _mm256_sub_pd(zero, t0);
                    double4 mask = _mm256_cmp_pd(nt0, t1, _CMP_LE_OQ);
                    active = _mm256_and_pd(active, mask);

                    //apply mask
                    t0 = _mm256_and_pd(t0, active);
                    t1 = _mm256_and_pd(t1, active);

                    //calculate normalized impact parameter
                    double4 nb;
                    impactparameter(packet, tree.spheres, idx, t0, t1, nb);
                    nb = _mm256_and_pd(nb, active);

                    //calculate weight
                    double4 weights;
                    kernel(nb, weights);
                    double4 ir2 = _mm256_set1_pd(tree.spheres.ir2[idx]);
                    weights = _mm256_mul_pd(weights, ir2);
                    weights = _mm256_and_pd(weights, active);
                    cdensity = _mm256_add_pd(cdensity, weights);
                }
            }

            //write column densities into image
            double dts[4]; _mm256_storeu_pd(dts, cdensity);
            image[ix  +sx* iy]    = log(dts[0]);
            image[ix+1+sx* iy]    = log(dts[1]);
            image[ix  +sx*(iy+1)] = log(dts[2]);
            image[ix+1+sx*(iy+1)] = log(dts[3]);
        }
    }

    //std::stringstream str;
    //str << "packet" << rank << ".pbm";

    //image.write(str.str());

    //MPI assembly
    if(rank == 0) {
        Image output(sx, sy);

        //write rank 0 data
        for(uint32_t iy = 0; iy < sy/(uint32_t)ranks; ++iy) {
            for(uint32_t ix = 0; ix < sx; ++ix) {
                output.setValue(ix, iy, image[ix+sx*iy]);
            }
        }

        //receive from all ranks
        for(int rank = 1; rank < ranks; ++rank) {
            MPI_Recv((void*)image, si, MPI_DOUBLE, rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            //write rank data
            int yof = rank*sy/(uint32_t)ranks;
            for(uint32_t iy = 0; iy < sy/(uint32_t)ranks; ++iy) {
                for(uint32_t ix = 0; ix < sx; ++ix) {
                    output.setValue(ix, (iy+yof), image[ix+sx*iy]);
                }
            }
        }

        output.write("mpi.pbm");
    }
    else{
        //send to rank 0
        MPI_Send((void*)image, si, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD); 
    }

    delete[] image;

    MPI_Finalize();
    return 0;*/
}

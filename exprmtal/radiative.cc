#include <string>
#include <cassert>
#include <cmath>

#include <omp.h>

#include "BVH.h"
#include "Kernel.h"
#include "Parser.h"
#include "Image.h"

#define Y 0.274
#define XHI 1.0
#define mH 1.66e-27
#define invmH 1.0/mH
#define XHImY (XHI*(1.0-Y))

#define sx 500
#define sy 500

int main(int argc, char** argv) {
    /*Spheres sphs;
    parse("aux/data/Data_025", sphs);
    std::cout << "parse" << std::endl;

    //generate tree
    BVH tree;
    mkSAHBVH(sphs, tree);
    std::cout << "build" << std::endl;

    //computation
    Image image(sx, sy);

    leafs_t leafs;
    leafs.reserve(500);

    Vector3 source(5000);

    std::cout << "calculating" << std::endl;

    for(size_t sidx = 0; sidx < tree.spheres.count; ++sidx) {
    //for(size_t sidx = 0; sidx < 500000; ++sidx) {
        if(sidx % 10000 == 0) printf("state %li\n", (uint64_t)sidx);

        //generate ray
        Vector3 sphc;
        tree.spheres.getCenter(sidx, sphc);

        Ray ray;
        points2ray(sphc, source, ray);

        //broadphase
        leafs.clear();
        tree.traverse(ray, leafs);

        double ncolumn = 0.0;
        double errcom = 0.0;

        //narrowphase
        for(uint32_t ileaf = 0; ileaf < leafs.size(); ++ileaf) {
            BVHLeaf lf = leafs[ileaf];

            //iterate spheres in leaf
            for(uint32_t inc = 0; inc < lf.run; ++inc) {
                uint32_t idx = lf.offset + inc;

                //ignore outgoing sphere
                if(idx == sidx) continue;

                //intersect
                real_t t0, t1;
                if(!tree.spheres.intersect(idx, ray, t0, t1)) 
                    continue;

                //filter
                //XXX prescision errors! use epsilon
                if((t0 == tree.spheres.r[idx]) || 
                   (t1 == tree.spheres.r[idx])) {
                    printf("flag\n");
                    continue; //XXX flag
                }
                //XXX VERIFY
                else if(-t0 > t1) { 
                    //printf("s skip\n");
                    continue;
                }

                //calculate normalized impact parameter
                real_t tp = (t0 + t1) * 0.5;
                Vector3 bv;
                ray(tp, bv);
                bv.com[0] -= tree.spheres.cx[idx];
                bv.com[1] -= tree.spheres.cy[idx];
                bv.com[2] -= tree.spheres.cz[idx];
              
                //TODO make lookup table f(b2/h2)
                real_t nb = bv.length() * tree.spheres.ir[idx];
                
                //calculate weight
                //real_t weight = kernel(nb);
                //fixes it! close, 3.44709e22
                real_t weight = kernel(nb);
                weight *= (tree.spheres.ir2[idx]);

                //calculate column contribution
                //kahan summation
                double y = weight - errcom;
                double t = ncolumn + y;
                errcom = (t - ncolumn) - y;
                ncolumn = t;
            }
        }

        ncolumn *= (8.0 * invmH * XHImY * sphs.mass);

        //XXX write into spherical image 
        double theta, phi;
        //[0,pi]
        theta = acos(ray.direction.com[2]); 
        //[0,2pi]
        phi = atan(ray.direction.com[1]/ray.direction.com[0]);

        size_t px, py;
        px = phi * (sx-1) * 0.5 / M_PI;
        py = theta * (sy-1) / M_PI;

        image.setValue(px, py, log(ncolumn));
    }

    std::cout << "writing" << std::endl;
    image.write("rad.pnm");
    std::cout << "done" << std::endl;*/
}

#include <omp.h>

#include "BVH.h"
#include "Kernel.h"
#include "Parser.h"
#include "Image.h"

#include <sstream>

#define sx 100
#define sy 100

int main(int argc, char** argv) {
    /*Spheres sphs;
    //parse("aux/data/Data_025", sphs);
    sphs.count = 1;
    sphs.allocate();
    sphs.cx[0] = 0;
    sphs.cy[0] = 0;
    sphs.cz[0] = 0;
    sphs.r[0]  = 10;

    sphs.fill();

    //generate tree
    BVH tree;
    mkSAHBVH(sphs, tree);
    sphs.deallocate();

    Vector3 min(-10,-10,-10), max(10,10,10);
    Vector3 center(0,0,0);

    for(uint32_t frame = 0; frame < 240; ++frame) {
        std::stringstream filename;
        filename << "tmp/frame" << frame << ".pgm";
        std::clog << filename.str() << std::endl;

        Image image(sx, sy);

        double t = 1.0/240.0 * frame * 3.14 * 2.0;

        Vector3 porigin;
        porigin.com[0] = 20 * cos(t);
        porigin.com[2] = 20 * sin(t);
        porigin.com[1] = center.com[1];

        Vector3 direction = center;
        direction -= porigin;
        direction.normalize();

        Vector3 up(0, 1, 0);
        Vector3 right = up.cross(direction);

        #pragma omp parallel
        {
            leafs_t leafs;
            leafs.reserve(500);

            #pragma omp for schedule(static,1)
            for(uint64_t v = 0; v < sy; ++v) {
                for(uint64_t u = 0; u < sx; ++u) {
                    //generate ray
                    real_t px = (2.0*u - sx)/(double)sx * 15.0;
                    real_t py = (2.0*v - sy)/(double)sy * 15.0;

                    Ray ray; 
                    ray.direction = direction;
                    ray.tmin = 0.0;
                    ray.tmax = 15000.0;

                    ray.origin = porigin;
                    ray.origin += (right*px);
                    ray.origin += (up*py);

                    //broadphase
                    leafs.clear();
                    tree.traverse(ray, leafs);

                    //narrowphase
                    real_t cdensity = 0.0;
                    for(uint32_t ileaf = 0; ileaf < leafs.size(); ++ileaf) {
                        BVHLeaf lf = leafs[ileaf];

                        //iterate spheres in leaf
                        //loop unroll? TODO
                        for(uint32_t inc = 0; inc < lf.run; ++inc) {
                            uint32_t idx = lf.offset + inc;
                            
                            //intersect
                            real_t t0, t1;
                            if(!tree.spheres.intersect(idx, ray, t0, t1)) 
                                continue;

                            //filter
                            //precision problems
                            if((t0 == tree.spheres.r[idx]) || 
                               (t1 == tree.spheres.r[idx])) {
                                //printf("flag\n");
                                continue; //XXX flag
                            }
                            else if(-t0 > t1) {
                                //printf("sph skipped\n");
                                continue;
                            }

                            //calculate normalized impact parameter
                            //TODO optimize, 17 
                            real_t tp = (t0 + t1) * 0.5;

                            //Vector3 bv;
                            //ray(tp, bv);

                            double bx, by, bz;
                            bx = tp * ray.direction.com[0];
                            by = tp * ray.direction.com[1];
                            bz = tp * ray.direction.com[2];
                            
                            bx += (ray.origin.com[0] - tree.spheres.cx[idx]);
                            by += (ray.origin.com[1] - tree.spheres.cy[idx]);
                            bz += (ray.origin.com[2] - tree.spheres.cz[idx]);

                            double len2 = (bx*bx) + (by*by) + (bz*bz);
                            double len = sqrt(len2);
                            double nb = len * tree.spheres.ir[idx];
                            
                            //calculate weight
                            double kern = kernel(nb);
                            cdensity += (kern * tree.spheres.ir2[idx]);
                        }
                    }

                    image.setValue(u, sy - v-1, log(cdensity));
                }
            }
        }
        
        //write out image
        image.write(filename.str());
    }*/
}

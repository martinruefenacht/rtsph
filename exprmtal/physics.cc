#include <omp.h>

#include "BVH.h"
#include "Kernel.h"
#include "Parser.h"
#include "Image.h"

#define sx 500
#define sy 500

#define Y 0.274
#define XHI 1.0
#define mH 1.66e-27
#define invmH 1.0/mH
#define XHImY (XHI*(1.0-Y))

/*double trace(const Ray& ray, const BVH& tree, leafs_t& leafs) {
    //broadphase
    leafs.clear();
    tree.traverse(ray, leafs);

    //narrowphase
    real_t cdensity = 0.0;
    for(uint32_t ileaf = 0; ileaf < leafs.size(); ++ileaf) {
        BVHLeaf lf = leafs[ileaf];

        //iterate spheres in leaf
        for(uint32_t inc = 0; inc < lf.run; ++inc) {
            uint32_t idx = lf.offset + inc;
            
            //intersect
            real_t t0, t1;
            if(!tree.spheres.intersect(idx, ray, t0, t1)) 
                continue;

            //filter
            //precision problems
            if((t0 == tree.spheres.r[idx]) || 
               (t1 == tree.spheres.r[idx])) {
                printf("flag\n");
                continue; //flag
            }
            else if(-t0 > t1) {
                printf("sph skipped\n");
                continue;
            }

            //calculate normalized impact parameter
            //TODO optimize, 17 
            real_t tp = (t0 + t1) * 0.5;
            Vector3 bv;
            ray(tp, bv);
            bv.com[0] -= tree.spheres.cx[idx];
            bv.com[1] -= tree.spheres.cy[idx];
            bv.com[2] -= tree.spheres.cz[idx];
            real_t nb = bv.length() * tree.spheres.ir[idx];
            //real_t nb = bv.length2() * tree.spheres.ir2[idx];
            
            //calculate weight
            double kern = kernel(nb);
            //XXX MISSING 2PI or 8!!!
            cdensity += (kern * tree.spheres.ir2[idx]);
        }
    }

    return cdensity;
}*/

int main(int argc, char** argv) {
    /*double start, end;

    std::clog << "parsing" << std::endl;
    Spheres sphs;
    start = omp_get_wtime();
    parse("aux/data/Data_025", sphs);
    end = omp_get_wtime();
    std::clog << "parse " << (end-start) << std::endl;

    //generate tree
    std::clog << "building" << std::endl;
    BVH tree;
    start = omp_get_wtime();
    mkSAHBVH(sphs, tree);
    end = omp_get_wtime();
    std::clog << "build " << (end-start) << std::endl;
    sphs.deallocate();

    //computation
    std::clog << "calculating" << std::endl;

    double avgcol = 0.0;
    double st = omp_get_wtime();
    #pragma omp parallel
    {
        Vector3 min(0, 0, 0);
        Vector3 max(10000, 10000, 10000);
        Vector3 dir(0.0, 0.0, 1.0);
        
        Vector3 d = max; d -= min;

        leafs_t leafs;
        leafs.reserve(500);

        double cdensity = 0.0;
        #pragma omp for schedule(static,1)
        for(uint64_t iy = 0; iy < sy; ++iy) {
            for(uint64_t ix = 0; ix < sx; ++ix) {
                //generate ray
                real_t px = (ix+0.5)/(double)sx;
                real_t py = (iy+0.5)/(double)sy;

                Ray ray; 
                ray.direction = dir;
                ray.origin = min;
                ray.tmin = 0.0;
                ray.tmax = 15000.0;
                
                ray.origin.com[0] += d.com[0] * px;
                ray.origin.com[1] += d.com[1] * py;

                //generate 4 more rays shifted in all directions!!!
                Ray lr, rr, tr, br;
                lr = ray; rr = ray;
                tr = ray; br = ray;
                lr.origin.com[0] -= 10000.0;
                rr.origin.com[0] += 10000.0;
                tr.origin.com[1] += 10000.0;
                br.origin.com[1] -= 10000.0;


                //periodic
                cdensity += trace(ray, tree, leafs);
                cdensity += trace(lr, tree, leafs);
                cdensity += trace(rr, tree, leafs);
                cdensity += trace(br, tree, leafs);
                cdensity += trace(tr, tree, leafs);
            }
        }
    
        #pragma omp critical
        {
            avgcol += cdensity;
        }
    }
    double en = omp_get_wtime();

    avgcol /= (sx*sy);
    avgcol *= (invmH * XHImY * sphs.mass * 8);

    std::clog << "outputing" << std::endl;
    std::cout << avgcol << std::endl;
    printf("TT  %0.6f\n", (en-st));
    printf("TPR %0.6f\n", ((en-st)/((double)sx*(double)sy)));*/
}

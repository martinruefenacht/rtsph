
#include "Parser.h"
#include "BVH.h"

#include <omp.h>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <cassert>

#include <unistd.h>

//requires work load division
//#define res 512

#define res 16
//#define res 256

/*void computeBBox(spheres_ct spheres, SmitAABB& bbox) {
    for(uint32_t idx = 0; idx < spheres.size(); ++idx) {
        const Sphere& sph = spheres[idx];
        Vector3 r3(sph.radius,sph.radius,sph.radius);
        Vector3 min = sph.center - r3;
        Vector3 max = sph.center + r3;

        bbox.expand(min, max);
    }
}*/

int main(int argc, char** argv) {
    //assert(argc == 2);

    /*long pages = sysconf(_SC_PHYS_PAGES);
    long page_size = sysconf(_SC_PAGE_SIZE);
    std::clog << "mem: " << pages*page_size << std::endl;*/

    //parse
    /*spheres_t spheres;
    parse(argv[1], spheres);

    std::clog << "res: " << res << std::endl;
    std::clog << "sphere mem: " << sizeof(Sphere)*spheres.size() << std::endl;
    
    //bounding box
    SmitAABB bbox;
    computeBBox(spheres,bbox);

    //generate rays
    std::clog << "ray generation" << std::endl;
    std::clog << sizeof(Ray) << std::endl;
    Vector3 center = (bbox.min + bbox.max) * 0.5;
    std::vector<Ray> rays;
    for(uint32_t x = 0; x < res; ++x) {
        for(uint32_t y = 0; y < res; ++y) {
            for(uint32_t z = 0; z < res; ++z) {
                Vector3 t, invt;
                t = Vector3(x/(real_t)res,y/(real_t)res,z/(real_t)res);
                invt = Vector3(1.0) - t;

                Vector3 out = bbox.min.cmul(invt) + bbox.max.cmul(t);
//                std::cout<<"("<<out.x<<" "<<out.y<<" "<< out.z<<")"<<std::endl;
                
                Ray ray;
                points2ray(center, out, ray);
                rays.push_back(ray);
            }
        }
    }

    std::clog << "ray mem: " << sizeof(Ray)*rays.size() << std::endl;

    double start, end;
    uint64_t count;

    //use Brute
    /*std::cout << "----------Brute----------" << std::endl;
    
    start = omp_get_wtime();
    
    Brute ntree;
    mkBrute(spheres, ntree);

    end = omp_get_wtime();
    std::cout << "Brute c: " << (end-start) << " s" << std::endl;

    count = 0;
    start = omp_get_wtime();
    #pragma omp parallel shared(ntree, rays) reduction(+:count)
    {
        #pragma omp for schedule(static,1)
        for(uint32_t idx = 0; idx < rays.size(); ++idx) {
            sphints_t intersects;
            ray_ct& ray = rays[idx];

            ntree.traverse(ray, intersects);

            count += intersects.size();
        }
    }
    end = omp_get_wtime();
    std::cout << "Brute t: " << (end-start) << " s" << std::endl;
    std::cout << "Brute r: " << (end-start)/rays.size() << " s" << std::endl;
    std::cout << "Brute i: " << count << std::endl;*/

    //use SAHBVH
    /*std::cout << "----------SBVH-----------" << std::endl;

    start = omp_get_wtime();

    BVH sbvh;
    mkSAHBVH(spheres, sbvh);
    //mkOMedBVH(spheres, sbvh);

    end = omp_get_wtime();
    std::cout << "SBVH c: " << (end-start) << " s" << std::endl;
    std::cout << "SBVH ns: " << sbvh.nodes.size()*sizeof(BVHNode) << std::endl;
    std::cout << "SBVH nr: " << sbvh.refs.size()*sizeof(uint32_t) << std::endl;

    count = 0;
    start = omp_get_wtime();
    #pragma omp parallel shared(sbvh, rays) reduction(+:count)
    {
        #pragma omp for schedule(static,1)
        for(uint32_t idx = 0; idx < rays.size(); ++idx) {
            sphints_t intersects;
            ray_ct& ray = rays[idx];

            sbvh.traverse(ray, intersects);

            count += intersects.size();
        }
    }
    end = omp_get_wtime();
    std::cout << "SBVH t: " << (end-start) << " s" << std::endl;
    std::cout << "SBVH r: " << (end-start)/rays.size() << " s" << std::endl;
    std::cout << "SBVH i: " << count << std::endl;
    std::cout << "SBVH ai: " << (double)count/(double)rays.size() << std::endl;
    std::cout << "SBVH ar: " << (double)count/(end-start) << std::endl;*/
}

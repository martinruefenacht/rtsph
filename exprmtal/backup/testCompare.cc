#include "Parser.h"
#include "BVH.h"

#include <omp.h>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <cassert>

/*void computeBBox(spheres_ct spheres, SmitAABB& bbox) {
    for(uint32_t idx = 0; idx < spheres.size(); ++idx) {
        const Sphere& sph = spheres[idx];
        Vector3 r3(sph.radius,sph.radius,sph.radius);
        Vector3 min = sph.center - r3;
        Vector3 max = sph.center + r3;

        bbox.expand(min, max);
    }
}*/

float randf() {
    return (float)rand()/(float)RAND_MAX;
}

int main(int argc, char** argv) {
    /*assert(argc == 2);

    //Parse
    spheres_t spheres;
    parse(argv[1], spheres);
    
    //Calculate bounding box
    SmitAABB bbox;
    computeBBox(spheres,bbox);

    //Rays
    Ray ray;
    points2ray(bbox.min, Vector3(bbox.max.x,bbox.max.y,0), ray);

    //Extend beyond AABB
    ray.origin = ray.origin - ray.direction*10.0;
    ray.tmax += 20.0;
   
    //Timing
    double start, end;
    //double time;

    std::cout << "------Naive------" << std::endl;
    Brute ntree;

    start = omp_get_wtime();
    mkBrute(spheres, ntree);
    end = omp_get_wtime();
    std::cout << "C " << (end-start) << " s" << std::endl;

    sphints_t nints;
    start = omp_get_wtime();
    ntree.traverse(ray, nints);
    end = omp_get_wtime();
    std::cout << "T " << (end-start) << " s" << std::endl;
    std::cout << "Total: " << nints.size() << std::endl;

    std::cout << "------CBVH-------" << std::endl;
    BVH cbvh;

    start = omp_get_wtime();
    mkOMedBVH(spheres, cbvh);
    end = omp_get_wtime();
    std::cout << "C " << (end-start) << " s" << std::endl;

    sphints_t cints;
    start = omp_get_wtime();
    cbvh.traverse(ray, cints);
    end = omp_get_wtime();
    std::cout << "T " << (end-start) << " s" << std::endl;
    std::cout << "Total: " << cints.size() << std::endl;

    std::cout << "------SBVH-------" << std::endl;
    BVH sbvh;

    start = omp_get_wtime();
    mkSAHBVH(spheres, sbvh);
    end = omp_get_wtime();
    std::cout << "C " << (end-start) << " s" << std::endl;

    sphints_t sints;
    start = omp_get_wtime();
    sbvh.traverse(ray, sints);
    end = omp_get_wtime();
    std::cout << "T " << (end-start) << " s" << std::endl;
    std::cout << "Total: " << sints.size() << std::endl;*/
}

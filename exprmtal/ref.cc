#include <omp.h>

#include "Parser.h"
#include "Kernel.h"
#include "Image.h"

#define sx 400
#define sy 400

int main(int argc, char** argv) {
    //std::clog << "parsing" << std::endl;
    //Spheres sphs;
    //parse("aux/data/Data_025", sphs);

    //computation
    std::clog << "calculating" << std::endl;
    //Image image(sx, sy);

    //double st = omp_get_wtime();

    Vector3 min(0, 0, 0);
    Vector3 max(10000, 10000, 10000);
    Vector3 dir(0.0, 0.0, 1.0);
    
    Vector3 d = max; d -= min;

    for(uint64_t iy = 0; iy < sy; ++iy) {
        for(uint64_t ix = 0; ix < sx; ++ix) {
            //generate ray
            double px = (ix+0.5)/(double)sx;
            double py = (iy+0.5)/(double)sy;

            Ray ray; 
            ray.direction = dir;
            ray.origin = min;
            ray.tmin = 0.0;
            ray.tmax = 15000.0;

            //perturb origin
            ray.origin.x() += d.x() * px;
            ray.origin.y() += d.y() * py;

            //narrowphase
            /*double cdensity = 0.0;
            for(uint32_t isph = 0; isph < sphs.count; ++isph) {
                //intersect
                double t0, t1;
                if(!sphs.intersect(isph, ray, t0, t1)) 
                    continue;

                //filter
                if(((t0*t0) == sphs.r2[isph]) || 
                   ((t1*t1) == sphs.r2[isph])) {
                    printf("ray flagged\n");
                    continue; //XXX flag
                }
                else if(-t0 > t1) {
                    printf("sph skipped\n");
                    continue;
                }

                //calculate normalized impact parameter
                double tp = (t0 + t1) * 0.5;
                Vector3 bv;
                ray(tp, bv);
                bv.com[0] -= sphs.cx[isph];
                bv.com[1] -= sphs.cy[isph];
                bv.com[2] -= sphs.cz[isph];
                double nb = bv.length() * sphs.ir[isph];
                
                //calculate weight
                cdensity += (kernel(nb) * (sphs.ir2[isph]));
            }*/

            //cdensity *= XHImY * sphs.mass;
            //image.setValue(ix, iy, log(cdensity));
        }
    }
    //double en = omp_get_wtime();

    //std::clog << "outputing" << std::endl;
    //image.write("ref.pbm");
    //printf("TT  %0.6f\n", (en-st));
    //printf("TPR %0.6f\n", ((en-st)/((double)sx*(double)sy)));
}

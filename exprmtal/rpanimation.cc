#include <string>
#include <cassert>
#include <cmath>

#include <omp.h>

#include "Kernel.h"
#include "Parser.h"
#include "Image.h"
#include "Worker.h"

#include <sstream>

#define sx 200
#define sy 200

int main(int argc, char** argv) {
    /*Spheres sphs;
    parse("aux/data/Data_025", sphs);

    Vector3 min(0,0,0), max(10000,10000,10000);
    Vector3 center(5000,5000,5000);
    
    for(uint32_t frame = 0; frame < 240; ++frame) {
        std::stringstream filename;
        filename << "rframe" << frame << ".pgm";

        double t = 1.0 / 240.0 * frame * 3.14 * 2.0;

        Image image(sx, sy);

        Vector3 porigin;
        porigin.com[0] = 5000 + 1000 * cos(t);
        porigin.com[1] = 5000 + 1000 * sin(t);
        porigin.com[2] = 5000;

        Vector3 direction = center;
        direction -= origin;
        direction.normalize();

        //partial?
        double4 dx, dy;
        dx = _mm256_set1_pd(d.com[0]);
        dy = _mm256_set1_pd(d.com[1]);

        #pragma omp parallel for schedule(static,1)
        for(uint64_t iy = 0; iy < sy; iy += 2) {
            for(uint64_t ix = 0; ix < sx; ix += 2) {
                //generate ray packet
                double pxh = (ix+0.5)/(double)sx;
                double pyh = (iy+0.5)/(double)sy;
                double pxf = (ix+1.5)/(double)sx;
                double pyf = (iy+1.5)/(double)sy;

                RPacket packet;
                //packet origin
                packet.ox = _mm256_set1_pd(min.com[0]);
                packet.oy = _mm256_set1_pd(min.com[1]);
                packet.oz = _mm256_set1_pd(min.com[2]);
                __m256d ptx = _mm256_setr_pd(pxh, pxf, pxh, pxf);
                __m256d pty = _mm256_setr_pd(pyh, pyh, pyf, pyf);
                __m256d offx = _mm256_mul_pd(dx, ptx);
                __m256d offy = _mm256_mul_pd(dy, pty);
                packet.ox = _mm256_add_pd(packet.ox, offx);
                packet.oy = _mm256_add_pd(packet.oy, offy);
                
                //packet directions
                packet.dx = _mm256_set1_pd(0.0);
                packet.dy = _mm256_set1_pd(0.0);
                packet.dz = _mm256_set1_pd(1.0);

                //packet length
                packet.tmin = _mm256_set1_pd(0.0);
                packet.tmax = _mm256_set1_pd(15000.0);

                //narrowphase
                double4 cdensity = zero;
                for(uint32_t isph = 0; isph < sphs.count; ++isph) {
                    //intersect sphere
                    PIntersect itr;
                    itr = sphs.intersect(isph, packet);

                    //filter
                    double4 nt0 = _mm256_sub_pd(zero, itr.t0);
                    double4 mask = _mm256_cmp_pd(nt0, itr.t1, _CMP_LE_OQ);
                    itr.active = _mm256_and_pd(itr.active, mask);

                    //check if any active
                    if(_mm256_movemask_pd(itr.active) == 0) continue;

                    itr.t0 = _mm256_and_pd(itr.t0, itr.active);
                    itr.t1 = _mm256_and_pd(itr.t1, itr.active);

                    //impact parameter
                    double4 nb = impactparameter(packet, sphs, isph, itr.t0, itr.t1);
                    nb = _mm256_and_pd(nb, itr.active);
                    
                    //weight
                    double4 kern = kernel(nb);
                    double4 ir2 = _mm256_set1_pd(sphs.ir2[isph]);
                    double4 weight = _mm256_mul_pd(kern, ir2);
                    weight = _mm256_and_pd(weight, itr.active);

                    cdensity = _mm256_add_pd(cdensity, weight);
                }

                double dts[4]; _mm256_storeu_pd(dts, cdensity);

                image.setValue(ix  , iy  , log(dts[0]));
                image.setValue(ix+1, iy  , log(dts[1]));
                image.setValue(ix  , iy+1, log(dts[2]));
                image.setValue(ix+1, iy+1, log(dts[3]));
            }
        }

        image.write("refpac.pbm");
    }*/
}

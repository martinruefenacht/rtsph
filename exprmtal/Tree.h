#pragma once

#include <vector>

#include "Common.h"
#include "Ray.h"
#include "Sphere.h"

struct SphereIntersect {
    sphref_t ref;
    real_t t0, t1;
};

typedef SphereIntersect sphint_t;
typedef std::vector<sphint_t> sphints_t;

///Trees store everything related to the tree.
///Stores references to spheres, not spheres themselves.
/*class Tree {
public:
    ///Traverses the tree structure.
    ///returns unique list of sphref_t of full & partial intersect
    virtual void traverse(ray_ct&, sphints_t&) const =0;

    ///Debug utility
    virtual void print() const =0;
};*/

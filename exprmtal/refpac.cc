#include <string>
#include <cassert>
#include <cmath>

#include <omp.h>

#include "Kernel.h"
#include "Parser.h"
#include "Image.h"
#include "Worker.h"

#define sx 100
#define sy 100

int main(int argc, char** argv) {
    /*std::clog << "parsing" << std::endl;
    Spheres sphs;
    parse("aux/data/Data_025", sphs);

    //computation
    Image image(sx, sy);

    std::cout << "calculating" << std::endl;
    double st = omp_get_wtime();

    Vector3 min(0,0,0);
    Vector3 max(10000,10000,10000);
    Vector3 dir(0.0, 0.0, 1.0);
    
    Vector3 d = max; d -= min;

    double4 dx, dy;
    dx = _mm256_set1_pd(d.com[0]);
    dy = _mm256_set1_pd(d.com[1]);

    for(uint64_t iy = 0; iy < sy; iy += 2) {
        for(uint64_t ix = 0; ix < sx; ix += 2) {
            //generate ray packet
            double pxh = (ix+0.5)/(double)sx;
            double pyh = (iy+0.5)/(double)sy;
            double pxf = (ix+1.5)/(double)sx;
            double pyf = (iy+1.5)/(double)sy;

            RPacket packet;
            //packet origin
            packet.ox = _mm256_set1_pd(min.com[0]);
            packet.oy = _mm256_set1_pd(min.com[1]);
            packet.oz = _mm256_set1_pd(min.com[2]);
            __m256d ptx = _mm256_setr_pd(pxh, pxf, pxh, pxf);
            __m256d pty = _mm256_setr_pd(pyh, pyh, pyf, pyf);
            __m256d offx = _mm256_mul_pd(dx, ptx);
            __m256d offy = _mm256_mul_pd(dy, pty);
            packet.ox = _mm256_add_pd(packet.ox, offx);
            packet.oy = _mm256_add_pd(packet.oy, offy);
            
            //packet directions
            packet.dx = _mm256_set1_pd(0.0);
            packet.dy = _mm256_set1_pd(0.0);
            packet.dz = _mm256_set1_pd(1.0);

            //packet length
            packet.tmin = _mm256_set1_pd(0.0);
            packet.tmax = _mm256_set1_pd(15000.0);

            //narrowphase
            double4 cdensity = zero;
            for(uint32_t isph = 0; isph < sphs.count; ++isph) {
                //intersect sphere
                PIntersect itr;
                itr = sphs.intersect(isph, packet);

                //filter
                double4 nt0 = _mm256_sub_pd(zero, itr.t0);
                double4 mask = _mm256_cmp_pd(nt0, itr.t1, _CMP_LE_OQ);
                itr.active = _mm256_and_pd(itr.active, mask);

                //check if any active
                if(_mm256_movemask_pd(itr.active) == 0) continue;

                itr.t0 = _mm256_and_pd(itr.t0, itr.active);
                itr.t1 = _mm256_and_pd(itr.t1, itr.active);

                //impact parameter
                double4 nb = impactparameter(packet, sphs, isph, itr.t0, itr.t1);
                nb = _mm256_and_pd(nb, itr.active);
                
                //weight
                double4 kern = kernel(nb);
                double4 ir2 = _mm256_set1_pd(sphs.ir2[isph]);
                double4 weight = _mm256_mul_pd(kern, ir2);
                weight = _mm256_and_pd(weight, itr.active);

                cdensity = _mm256_add_pd(cdensity, weight);
            }

            double dts[4]; _mm256_storeu_pd(dts, cdensity);

            image.setValue(ix  , iy  , log(dts[0]));
            image.setValue(ix+1, iy  , log(dts[1]));
            image.setValue(ix  , iy+1, log(dts[2]));
            image.setValue(ix+1, iy+1, log(dts[3]));
        }
    }

    double en = omp_get_wtime();
    std::cout << std::endl;

    std::clog << "outputing" << std::endl;
    image.write("refpac.pbm");
    printf("TT  %0.6f\n", (en-st));
    printf("TPR %0.6f\n", ((en-st)/((double)sx*(double)sy)));*/
}

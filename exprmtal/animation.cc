#include <omp.h>

#include "BVH.h"
#include "Kernel.h"
#include "Parser.h"
#include "Image.h"

#include <sstream>

#define sx 500
#define sy 500

int main(int argc, char** argv) {
	/*double start = omp_get_wtime();
    Spheres sphs;
    parse("aux/data/Data_025", sphs);

    //generate tree
    BVH tree;
    mkSAHBVH(sphs, tree);
    sphs.deallocate();

    Vector3 min(0,0,0), max(10000,10000,10000);
    Vector3 center(5000,5000,5000);

    for(uint32_t frame = 153; frame < 154; ++frame) {
        std::stringstream filename;
        filename << "tmp/frame" << frame << ".pgm";
        std::clog << filename.str() << std::endl;

        Image image(sx, sy);

        double t = 1.0/240.0 * frame * 3.14 * 2.0;

        Vector3 porigin;
        porigin.com[0] = 5000 + 7500 * cos(t);
        porigin.com[1] = 5000 + 4500;
        porigin.com[2] = 5000 + 7500 * sin(t);

        Vector3 direction = center;
        direction -= porigin;
        direction.normalize();

		Vector3 up(0,1,0);
        Vector3 right = up.cross(direction);
		Vector3 nup = direction.cross(right);

		right.normalize();
		nup.normalize();

        #pragma omp parallel
        {
            leafs_t leafs;
            leafs.reserve(500);

            #pragma omp for schedule(static,1)
            for(uint64_t v = 0; v < sy; ++v) {
                for(uint64_t u = 0; u < sx; ++u) {
                    //generate ray
                    real_t px = (2.0*u - sx)/(double)sx * 8500.0;
                    real_t py = (2.0*v - sy)/(double)sy * 8500.0;

                    Ray ray; 
                    ray.direction = direction;
                    ray.tmin = 0.0;
                    ray.tmax = real_inf;

                    ray.origin = porigin;
                    ray.origin += (right*px);
                    ray.origin += (nup*py);

                    //broadphase
                    leafs.clear();
                    tree.traverse(ray, leafs);

                    //narrowphase
                    real_t cdensity = 0.0;
                    for(uint32_t ileaf = 0; ileaf < leafs.size(); ++ileaf) {
                        BVHLeaf lf = leafs[ileaf];

                        //iterate spheres in leaf
                        //loop unroll? TODO
                        for(uint32_t inc = 0; inc < lf.run; ++inc) {
                            uint32_t idx = lf.offset + inc;
                            
                            //intersect
                            real_t t0, t1;
                            if(!tree.spheres.intersect(idx, ray, t0, t1)) 
                                continue;

                            //filter
                            //precision problems
                            /*if((t0 == tree.spheres.r[idx]) || 
                               (t1 == tree.spheres.r[idx])) {
                                //printf("flag\n");
                                continue; //XXX flag
                            }
							//XXX not working
                            else if(-t0 > t1) {
                                //printf("sph skipped\n");
                                continue;
                            }*/

                            //calculate normalized impact parameter
                            /*real_t tp = (t0 + t1) * 0.5;
                            Vector3 b; ray(tp, b);
                            b.com[0] -= tree.spheres.cx[idx];
                            b.com[1] -= tree.spheres.cy[idx];
                            b.com[2] -= tree.spheres.cz[idx];

                            //XXX get rid of SQRT
                            double nb = b.length() * tree.spheres.ir[idx];
                            
                            //calculate weight
                            double kern = kernel(nb);
                            cdensity += (kern * tree.spheres.ir2[idx]);
                        }
                    }

                    image.setValue(u, v, log(cdensity));
                }
            }
        }
        
        //write out image
        image.write(filename.str());
    }
	double end = omp_get_wtime();
	std::clog << (end-start) << " s" << std::endl;*/
}

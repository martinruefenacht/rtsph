#!/usr/bin/gnuplot

set xlabel "Threads"
set ylabel "Time ({/Symbol m}s)"

set size 1,1

set format y '10^{%L}'
set logscale y 10
set grid

set xrange [0:33]

set term postscript eps enhanced color
set output "../graphics/thrperf.eps"

p "thrdata" u 1:13 w linespoints title "Estimated Brute Force", \
  "thrdata" u 1:5  w linespoints title "Rays", \
  "thrdata" u 1:11 w linespoints  title "Packets"

In this section optimizations which vastly improved performance,
however are not part of the core algorithm are presented. While
simple optimizations such as reducing multiplications in the
sphere-ray intersection method are not included here, more
important changes are included which required a certain way of 
programming the implementation which is a choice of the programmer.

\subsection{Data Layout}
The data layout is commonly dependent on the problem which is being
solved and usually decided by the programmer for understandablilty.
Due to the size of the basic structures used in the algorithm
some layouts and usages are more optimal. An Array of 
Structures(AOS) layout is defined by constructing a list of basic
structures in an array this method is simple to understand and can
be efficent for very small data types such as double precision 
floating point numbers. Ideally when the list is accessed in a 
sequential order then modern central processing units can predict
easily which elements are going to be accessed before they are
instructed to fetch the memory and can prefetch memory such that it
is already present when required. The opposite memory layout is
called Structure of Arrays(SOA). This is defined by not having 
individual structures for each basic structure, but to have a 
single structure which contains arrays of memory corresponding to 
elements within a structure. The benefit of the SOA layout is 
present when structures are of the size of the cachelines present
on the machines architecture. When an array is traversed 
sequentially with small size data then an individual cacheline 
contains many elements of data, however when the structure is close
the same size then the cache miss per data element is much greater,
therefore the memory access is much more expensive.

%AOS layout
The rays and packets in the current implementation are in the AOS
layout due to the infrequency of change. The ideal is to have the
entire ray or packet in a cacheblock which does not interfere with
the rest of the algorithm. The tree and particle data are large
enough to cause most of the cache to be overwritten in a single
tracing event. Additionally the tree data is also in an AOS layout
this was tested an shown to be equivalent, therefore the 
implementation was chosen to be as simple and understandable as 
possible. The equal performance is likely due to the non-sequential
traversal of the tree data. Since the order is not decided until the
specific ray or packet has visited the parent node of the tree, it
is not possible to know which node will be visited next.

%SOA layout
Contrary to the rays and tree data the particle data is laid out in
a SOA layout due to the sequentially traversing of the particle
data in the narrowphase of the algorithm. When the first particle of
a node is accessed then the data for the next couple particles is
also loaded into memory on the same cachelines as the first. By 
using this method the cache miss per particle is reduced 
substanitally from a cache miss per access to one cache miss every
four particles. Alongside the memory access prediction this method
allows very fast accessing of the particle memory.

\subsection{Lookup Table}
The linear interpolation descibed in section \ref{sec:cdc} is 
algorithmicly optimized to be able to give the fastest possible 
result. The only complex function required to evaluate the kernel
value is the floor function, which unfortunately is very slow
due to requirements placed on it by the IEEE 754 standard for 
floating point numbers\cite{ieee754}.

By using a standard cast to convert the double precision value of
the look up table index into an integer value by truncation an 
equivalent method is found. This method does not provide the safety
which the standard library floor function provides, however for
this particular situation is suitable and provides correct results.

\subsection{Parallelism}
%parallelizablility of the tree construction not used
The binned surface area heurstic build algorithm which was presented
in section \ref{sec:con} was not implemented as a parallel 
algorithm. The original publication\cite{bvhbin} of the algorithm 
did include strategies for parallelization, however they reported
disappointing scalablility with current hardware likely due to the
memory bandwidth required to access the entire dataset of geometric
primitives by multiple threads. The performance of the build 
algorithm is not very important due to the small fraction of overall
time actually spent performing the build, therefore the current
implementation does not include a parallelized build system.

\subsubsection{Instruction Level Parallelism}

While ray tracing is a very capable tool for graphics and 
geometric problems it is generally considered slow compared to 
other methods. Many approaches have been explored to allievate
the performance problems of ray tracing, however once trees are
utilized no other method comes close to having as much of a 
performance impact. Generally approaches only reduce the constant 
of the implementation and do not provide any algorithmic 
optimization which would result in orders of magnitude of 
performance.

Rays are very fundemental to ray tracing, however it is merely
tradition and simplicity that rays are used. Many other types of
geometric shapes have been considered such as beams or prisms.
Compared to rays other shapes are at a disadvantage, because the
difficulty of actually performing an intersection with these and
other shapes is very computationally expensive. Havran\cite{rayhur}
was the first to consider ray packets which mimick the behaviour of
prisms, but are still rays to be traced.

Ray packets are simply a collection of rays which when put together
form a tool for higher performance. The key element to ray packets
is that rays which are together in a packet must be coherent. By
coherent rays we mean that they are spatially close or would likely
share a high portion of the space that the rays travel through. For
ray tracing this means that if a ray packet is used with coherent
rays then the chance of hitting similar geometry is high and
therefore the cost of having to traverse the acceleration structure
and other overheads are shared between all the rays in the packet.
Ray packets shared the overhead of tracing rays through the data in
the hope that the cost is reduced sufficently such that it is 
cheaper to calculate many rays at the same time with more misses 
instead of calculating each ray exactly individually. 
Another motivation for ray packets is the Single Instruction 
Multiple Data (SIMD) instruction extensions which were added to the 
modern x86 instruction set in the late 1990s.

SIMD instructions as the name suggests allow multiple pieces of data
to be executed upon by the same instruction within the same clock
cycle. These instructions are similar to when supercomputers were 
still large vector machines, by processing as much data as possible
in parallel on the instruction level much performance could be
gained for the general algorithm in place, the caveat is that the
pieces of data are not allowed to be dependent on each other, 
otherwise the process has to revert to a entirely serial approach.

Ray packets can be implemented naively by simply replicating the
work that must be performed for each ray as if they were being
traced individually. This is only slightly more efficient than
normal ray tracing and does not utilize the modern hardware 
available. Since SIMD instructions allow independent rays to be
processed it is simply to allow ray packets to use those 
instructions and process at the same time the number of rays which
fit into the SIMD width of the hardware present
\cite{bvhavx,packettrac,lpackets}.

The implementation present in the source code can be used across all
modern SSE and AVX architectures. Using ray packets is highly 
advised since the problem of finding the column density along rays
is extremely suitable for parallelization. The difficulty with 
ray packets is to find an appropriate configuration such that the
overhead of traversing more nodes and particles is less than the
cost of not sharing the overhead of tracing all rays individually.

\subsection{Smoothed Particle Hydrodynamics} \label{sec:sph}

%intro to computational fluid dynamics
As described in section \ref{sec:int} cosmological simulations make 
use of fluid simulations for the gas dynamics in the simulation 
domain. Several methods exist for computational fluid dynamics which
can be split into two broad methods using mesh representation of a
fluid or using a particle of fluid element representation.

%mesh based
The mesh based approach to fluid simulation is a Eulerian method
which means that the sample points are fixed in the simulation 
domain. This approach allows easy determination of derivatives which
are required to calculate the fluid dynamics. While the accuracy 
of these simulations is good they also require extensive amount of 
memory and mesh refinement for varying density of fluids of large
size, therefore cosmological simulation usually make use of particle
based methods.

%sph intro
Smoothed particle hydrodynamics is a Lagrangian fluid simulation
which is based on samples traveling as fluid elements with the
fluid being simulated. Due to the Lagrangian nature of the method
conservation laws are easily upheld in comparison to mesh based
fluid simulations. In addition the particles allow automatic 
resolution refinement of volumes of the fluid in contrast to meshes
which need to be geometrically refined and adjusted which is a
difficult computational geometry problem. 

%simple equations
The smoothed particle hydrodynamics(SPH) formulation is entirely 
based on equation \ref{eq:sph}. With this equation and a set of 
particles it is possible to calculate any field value at any point 
assuming that a high enough particle density is present for an 
accurate interpolation. The \(W(\textbf{r},h)\) present in the 
equation is the kernel function which is used as the interpolating 
function.

\begin{equation} \label{eq:sph}
    F_s(\textbf{r}) = \int F(\textbf{r}) \: \\
        W(\textbf{r}-\textbf{r}^\prime,h) \: \\
        \mathrm{d}\textbf{r}^\prime
\end{equation}

%kernel
The kernel is fundamental to the function of the SPH method, because
it determines how much a particle influences the calculation of 
everything derived from the particle. Two conditions exist which
are required for the method to function as required as shown in 
equations \ref{eq:keri} and \ref{eq:kerc}.

\begin{equation} \label{eq:keri}
    \int W(\textbf{r},h) \: \mathrm{d}^3\textbf{r} = 1
\end{equation}
\begin{equation} \label{eq:kerc}
    \langle f(\textbf{r}) \rangle  = f(\textbf{r}) \\
    \quad \text{for}\quad h\to0
\end{equation}

Many different kernels exist which can be used as required, however 
it is beneficial to use a kernel with a finite support after which 
all contribution is zero. Additionally it is good to have a 
symmetric and smooth kernel such that it is differentiable twice.
Gaussian kernels which were first to be proposed will contribute to 
all particles, because they have non-zero kernel values at all 
positions. While that is correct behaviour the contribution is so
small that after a certain distance it is irrelevant due to other
particles. This project is based on the Gadget2 kernel\cite{gadget2}
shown in equation \ref{eq:gad} which is based in turn on the cubic 
spline kernel commonly used for smoothed particle hydrodynamics.

\begin{equation} \label{eq:gad}
    q = \frac{r}{h} \quad \quad
    k_{3D}(r,h) = \frac{8}{\pi} \left\{
        \begin{array}{l l}
            1 - 6q^2 + 6q^3 & 0 \le q \le \frac{1}{2}\\
            2(1-q)^3        & \frac{1}{2} < q \le 1\\
            0               & 1 < q
        \end{array} \right.
\end{equation}

By inspection equation \ref{eq:gad} it can be seen that any 
particles outside the smoothing radius \(h\) are not being 
contributed to. In essence the SPH method is a Monte-Carlo based
idea with non-random or quasi random sample points which are well
distributed such that the final result is acceptably accurate.
The simplicity of SPH methods lies in the fact that when a 
derivative of a field is required the only required derivative is
of the kernel function while the actual sample values are constant.

%density
Equation \ref{eq:dsph} is a discretized version of equation 
\ref{eq:sph} which is used to numerically integrate values at any
location. The slight modification present is used to weight how
much the particle contributes compared to other particles in respect
to their masses.

\begin{equation} \label{eq:dsph}
    F_s(\textbf{r}) \simeq \sum_j \frac{m_j}{\rho_j}F_j \\
        W(\textbf{r} - \textbf{r}_j,h)
\end{equation}

When the field to be evaluated is chosen to be the density field of
the simulation equation \ref{eq:dsph} becomes equation \ref{eq:rhof}
which is simply a calculation based on particles and their mass. 
Compared to the calculation of density in a mesh based approach this
is extremely simple especially considering that if kernels with 
finite support are used particles outside the smoothing length can
be ignored entirely which reduces the computational time
\cite{astsph}.

\begin{equation} \label{eq:rhof}
    \rho_s(\textbf{r}) \simeq \sum_j m_j \\ 
         W(\textbf{r}-\textbf{r}^\prime,h) 
\end{equation}

%astrophysics applications
By using all these properties of smoothed particle hydrodynamics
it has shown to be of great value for astrophysics simulations.
For cosmological gas simulations the pressure field is calculated
and the forces are accumulated alongside gravitation forces from the
n-body methods to form complex behaviour.

\subsection{Radiative Transfer methods}
%basic equations
Radiative transfer is the process by which energy is transferred
by electromagnetic radiation through a medium with which it 
interacts. Typically it is defined as the intensity of a frequency
which passes through a point at a certain time. The medium through
which it passes has an absorption, emission and scattering 
coefficent. In terms of cosmological simulations we are only 
interested in the absorption of the radiation since the gas does not
emit radiation and scattered photons which we do not observe as 
direct light is not important for understanding of what we observe.
The basic equation of radiative transfer is equation \ref{eq:rad}
where \(a\) is the absorption coefficent.

\begin{equation} \label{eq:rad}
    \frac{\mathrm{d}I_v}{\mathrm{d}s} = -a I_v
\end{equation}

Solutions to full radiative transfer equations are a elaborate 
field, however to equation \ref{eq:rad} solutions are of the form
shown in equation \ref{eq:sol}.

\begin{equation} \label{eq:sol}
    I_v = I_v(0) \exp{-a s}
\end{equation}

%column density incerases along sight line
From equation \ref{eq:sol} it is clear that the further through an
absorbing medium the intensity of the radiation will decrease
exponentially. Additionally the exponent \(-a s\) is commonly 
defined as the optical depth. When the medium is a non-homogenous
distribution the solution to this equation cannot be expressed
simply. The optical depth can be defined as equation \ref{eq:opt}.

\begin{equation} \label{eq:opt}
    \tau = \int_0^s a(s^\prime) \mathrm{d}s^\prime
\end{equation}

%how it affects the intensity of light absorbed through column
In turn the optical depth is linearly related to the column density
of the ray through the medium. Equation \ref{eq:col} shown this
relationship which implies that all that is required to be known
other then constants of the medium is the column density through
which the ray passes.

\begin{equation} \label{eq:col}
    a(s) = \sigma n s
\end{equation}

%ionisation and heating depend on intensity radiation field
The column density of a ray is the mass of the gas which is 
intersected by the ray per unit area along the ray. The column
density is the difficult quantity to calculate through a non-uniform
simulation. Using radiative transfer it is possible to calculate 
the ionization and heating of the gas in the intergalactic medium
from sources of ionizing radiation.

\begin{equation} \label{eq:ion}
    \Gamma = 4 \pi \int_{v_T}^{\infty} \frac{J_v}{hv}\sigma_v \\
        \mathrm{d}v
\end{equation}
\begin{equation} \label{eq:het}
    G = 4\pi n \int_{v_T}^{\infty} \frac{J_v\sigma_v}{v} \\
        (v-v_T) \mathrm{d}v
\end{equation}

Equation \ref{eq:ion} is the photoionisation rate per particle which
is dependent on the ionisation cross section \(\sigma\) and the 
local mean radiation field \(J_v\) of the species such as hydrogen
or helium. Equation \ref{eq:het} is the heating of the particle
which with the excess energy above the ionization energy for a given
species\cite{rtgray, rtspitzer, rtoster, reion}.

\subsection{Ray Tracing}

%what is ray tracing
Ray tracing is a well known problem which is used for finding a 
solution to geometric problems in spatial environments on a 
computer. By intersecting a ray with the geometry which is present 
in memory the closest object can be found or the distance to an 
object.

%computer graphics relevant
In computer graphics ray tracing\cite{pbrt} is a well known method
to create photo realistic images from virtual environments and is
currently being used for computer generated graphics used in movies.
The performance of ray tracing is not high enough currently to be
able to use it in a real time environment such as a game.

%problems with it
The ray tracing algorithm checks for intersections between all the 
geometry and the ray which it is processing. The effect of this is 
that for each ray a large number of objects must be tested with a 
computationally expensive method to determine whether an 
intersection is occurring and to calculate the information 
associated with the intersection. This process is of complexity 
\(O(N^2)\) when done for more than a single ray. For low numbers of
either rays or objects this is acceptable, however for large 
numbers this becomes entirely unfeasible. Several methods of 
accelerating the tracing of rays in the context of computer 
graphics have been researched alongside other uses which require 
ray tracing.

\subsubsection{Acceleration Structures}

%general idea of acceleration
Since ray tracing in the naive form of the algorithm is an 
\(O(N^2)\) process it is unfeasible to use for large numbers of rays
or particles. The number of rays can not be reduced considering it 
is the minimum required to solve a problem. Many forms of 
accelerating the tracing of rays have been researched which range 
from caching recent objects intersected to large tree structures
which reduce the problem set. 

%acceleration structures intro
Acceleration structures are the most significant change the ray 
tracing algorithm. Tree structures require a preprocessing phase on
the entire dataset which allows the building of the index structure
used at runtime by the ray tracing algorithm to evaluate the 
queries. Acceleration structures provide a fast method to evaluate 
whether a group of particles would possibly be intersected by a ray.
This allows fast exclusion of large numbers of particles which no 
longer need to be tested individually and therefore a large increase
in performance.

%spatially subdividing trees
A large number of trees have been invented which subdivide space 
such that if the ray does not intersect the section of space then 
the objects inside that space can also be excluded. Structures such
as regular grids and KD trees\cite{pbrt} are spatially subdividing 
trees. Regular grids are a simple scheme by which space is simply 
tessellated into a set of cubes into which all objects are sorted 
in the pre-process phase. When a ray traverses the grid each spatial
cube which is intersected has its objects tested for intersection.
Compared to KD trees the grid approach is very simple to understand.
The KD trees are spatial binary trees which subdivide the space into
half of the origin space at each level of the tree. Through this 
tree hierarchy the space can be extremely efficiently reduced. The 
state of the art acceleration structure currently is considered the
KD tree build with a surface area heuristic algorithm which is 
further explained in section \ref{sec:con}.
%downside grids are large overhead for each empty cell
%downside kd trees have large trees, lots of memory and large stacks
%downside spatial has duplicates of objects, requires pruning if intersecting
%more than one

%object subdividing trees
Contrary to spatially subdividing trees the objects can also be 
subdivided in a list form instead of spatially. By grouping objects
into volumes which can be efficiently tested we can decided whether
a ray should be tested against all the objects within the bounding 
volume. When the same process is done on the bounding volumes a 
hierarchy tree is formed and can be traversed similarity to a 
spatial subdivision tree. The most common approach for this method 
is the bounding volume hierarchy(BVH) with axis aligned bounding 
boxes as the enclosing volume which is tested quickly. Extensions 
on the original BVH have been research due to their performance 
especially on graphics hardware where spatial trees are no longer
as efficient.

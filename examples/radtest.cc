#include <omp.h>
#include "RTSPH.h"

int main(int argc, char** argv) {
    /*double start, end;
    std::cout << "parsing" << std::endl;
    Spheres sphs;
    start = omp_get_wtime();
    parse("aux/data/Data_025", sphs);
    end = omp_get_wtime();
    std::cout << "parsed " << (end-start) << std::endl;

    //generate tree
    std::cout << "building" << std::endl;
    BVH tree;
    start = omp_get_wtime();
    mkSAHBVH(sphs, tree);
    end = omp_get_wtime();
    std::cout << "built " << (end-start) << std::endl;
    sphs.erase();

    //generate rays
    std::cout << "generate rays" << std::endl;
    Vector3 center(5000.0);
	Rays rays;
    for(uint32_t idx = 0; idx < tree.spheres.count(); ++idx) {
        Vector3 sc; tree.spheres.getCenter(idx, sc);
        rays.setFromPoints(sc, center, idx);
    }

    //processing
    double sum = 0.0;

    start = omp_get_wtime();
    std::cout << "processing rays " << rays.count() << std::endl;
    #pragma omp parallel for schedule(static, 1)
    for(size_t iray = 0; iray < rays.count(); ++iray) {
        //fetch ray
        Ray ray; rays.get(iray, ray);

        //calculate column density
        double rayweight = tree.weightRay(ray);

        #pragma omp atomic
        sum += rayweight;
    }
    end = omp_get_wtime();
    std::cout << "processing done " << (end-start) << " s ";
    std::cout << std::endl;

    std::cout << "sum - " << sum << std::endl;*/
}

#include "Weight.h"
#include <iostream>

#define step 0.0005

int main() {
    //for(double nb2 = 0.0; nb2 <= 1.0; nb2 += 0.001) {
    for(double nb = 0.0; nb <= 1.0+step*4; nb += step) {
        double k2d;
		kernelnb2(nb, k2d);

        std::cout << nb << " ";
        std::cout << k2d << std::endl;
    }
}

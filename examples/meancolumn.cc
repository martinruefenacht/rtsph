#include <omp.h>

#include <fstream>

#include "Common.h"
#include "G2Dataset.h"
#include "BVH.h"
#include "Weight.h"
#include "Parser.h"
#include "Points.h"

#define sx 500
#define sy 500

#define Y 0.274
#define XHI 1.0
#define mH 1.66e-27
#define invmH 1.0/mH
#define XHImY (XHI*(1.0-Y))
#define HUBBLE 0.7
#define UnitMass 1.989e40
#define UnitLength 3.0857e19
#define REDSHIFT 3.0

int main(int argc, char** argv) {
    double start, end;
    std::cout << "parsing" << std::endl;
    G2Dataset data;
    Spheres sphs;
    start = omp_get_wtime();
    std::string filename("aux/data/Data_025");
    parseG2Dataset(filename , data);
    data.getSpheres(sphs);
    end = omp_get_wtime();
    std::cout << "parsed " << (end-start) << std::endl;

    //generate tree
    std::cout << "building" << std::endl;
    BVH tree;
    start = omp_get_wtime();
    mkSAHBVH(sphs, tree);
    end = omp_get_wtime();
    std::cout << "built " << (end-start) << std::endl;

    //copy masses and reorder
    double *masses = new double[data.npart[0]];
    for(uint32_t idx = 0; idx < data.npart[0]; ++idx) {
        masses[idx] = data.hmass[0];
    }

    //scale mass by hubble parameter from file
    //tree.spheres.hmass[0] *= (UnitMass / HUBBLE);
    for(size_t isph = 0; isph < tree.spheres.count(); ++isph) {
        //tree.spheres.q[isph] *= (UnitMass / HUBBLE);
        masses[isph] *= (UnitMass / HUBBLE);
    }

    //generate rays
    std::cout << "generate rays" << std::endl;
	Rays rays;
    for(uint32_t iy = 0; iy < sy; ++iy) {
        for(uint32_t ix = 0; ix < sx; ++ix) {
            //generate ray
            double px = (ix+0.5)/sx * 10000.0;
            double py = (iy+0.5)/sy * 10000.0;
            
            Vector3 p1, p2;
            
            //center
			p1.x() = px; p1.y() = py; p1.z() = -1000;
            p2.x() = px; p2.y() = py; p2.z() = 11000;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //left
            /*p1.x() -= 10000.0;
            p2.x() -= 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //ltop
            p1.y() += 10000.0;
            p2.y() += 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //top
            p1.x() += 10000.0;
            p2.x() += 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //rtop
            p1.x() += 10000.0;
            p2.x() += 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //right
            p1.y() -= 10000.0;
            p2.y() -= 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //bright
            p1.y() -= 10000.0;
            p2.y() -= 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //bottom
            p1.x() -= 10000.0;
            p2.x() -= 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);

            //lbottom
            p1.x() -= 10000.0;
            p2.x() -= 10000.0;
	     	rays.setFromPoints(p1, p2, ix+iy*sx);*/
		}
    }

    //processing
    start = omp_get_wtime();
    double total = 0.0;
    std::cout << "processing rays " << rays.count() << std::endl;
    #pragma omp parallel for schedule(static, 1)
    for(size_t iray = 0; iray < rays.count(); ++iray) {
        //fetch ray
        Ray ray; rays.get(iray, ray);

        //calculate column density
        double density = tree.weightRay(ray, masses);

        //insert
        double value = density * invmH * XHImY;

        // Change to column density
        value *= (1.0+REDSHIFT)*(1.0+REDSHIFT);
        value *= (HUBBLE*HUBBLE);
        value /= (UnitLength*UnitLength);

        #pragma omp atomic
        total += value;
    }
    end = omp_get_wtime();
    std::cout << "processing done " << (end-start) << " s ";
    std::cout << (end-start)*1000000.0/(sx*sy)<< " us";
    std::cout << std::endl;

    double mean = total / rays.count();
    std::cout << "mean column density - " << mean << std::endl;
}

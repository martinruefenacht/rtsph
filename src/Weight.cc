#include "Weight.h"

void cubicnb2(double nb2, double& weight) {
    //interpolation bounds search
    size_t ilow, ihigh;    
    ilow = (size_t)(nb2*range);
    ihigh = ilow + 1;

    //cubic hermite interpolation in quadratic space
    double t = (nb2 - nb2s[ilow]) * invdf;
    double t2 = t*t;
    double t3 = t*t2;

    double h00 = (2*t3) - (3*t2) + 1;
    double h10 = (t3 - (2*t2) + t) * df;
    double h01 = (-2*t3) + (3*t2);
    double h11 = (t3 - t2) * df;

    double pl = weights[ilow] * h00;
    double ml = dwdnb[ilow] * h10;

    double ph = weights[ihigh] * h01;
    double mh = dwdnb[ihigh] * h11;

    weight = (pl + ph) + (ml + mh);
}

/*void linearnb2(double nb2, double& weight) {
    //indicies
    size_t ilow, ihigh;
    ilow = (size_t)(nb2*1022.999999);
    ihigh = ilow + 1;

    //linear interpolation
    double intplt = (nb2-nb2s[ilow]) * invdf;

}*/

void kernelnb2(const double nbs1, double& kern) {
    //linearnb2(nbs1, kern);
    cubicnb2(nbs1, kern);
}

double kernelnb2(const double nbs1) {
	double kern;
	cubicnb2(nbs1, kern);
	return kern;
}

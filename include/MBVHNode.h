#pragma once

#define MBVHBF 4

//contains all children

struct MBVHNode {
    //bounding boxes of children
    AABB bboxs[4];

    uint32_t right[4];
};

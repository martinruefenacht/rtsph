#pragma once

#include <stack>
#include <vector>

#include "Common.h"
#include "BVHNode.h"
#include "Spheres.h"
#include "Ray.h"
#include "Packet.h"

#include "Weight.h"

struct BVH {
//access required for building
    Spheres spheres;
    std::vector<uint32_t> map;
    std::vector<BVHNode> nodes;

    /// \brief Calculates column density through the tree.
    ///
    /// This function calculates the total column density 
    /// accumulated by the given ray through the entire tree.
    //double columnDensityQ(const Ray&) const;
    //double columnDensityU(const Ray&) const;

    double weightRay(const Ray&, const double*) const;
    double weightRay(const Ray&) const;

    double weightNode(const Ray&, uint32_t, const double*) const;
    double weightNode(const Ray&, uint32_t) const;

    ///
    ///
    ///
    //double weightOfNodeQ(const Ray&, uint32_t) const;
    //double weightOfNodeU(const Ray&, uint32_t) const;
    
    /*#if defined __SIMD__
    ///
    ///
    ///
    inline void weightOfNode(const Packet&, uint32_t, doublew[PACKET_SIZE]) const;

    ///
    ///
    ///
    inline void columnDensity(const Packet&, doublew[PACKET_SIZE]) const;
    #endif*/
};

/// \brief Prototype of SAH BVH build factory.
///
/// This function builds a SAH BVH out of a collection of spheres.
void mkSAHBVH(const Spheres&, BVH&);



#pragma once

#include "nanoflann.hpp"
using namespace nanoflann;

#include <cstring>

#include "Rays.h"
#include "Packet.h"

#ifdef __SIMD__
struct Point {
    double coords[6];
    uint64_t id;
};

struct PointCloud {
    std::vector<Point> points;

    inline size_t kdtree_get_point_count() const {
        return points.size();
    }

    inline double kdtree_distance(const double* p1, const size_t idx, size_t size) const {
        double d0 = p1[0] - points[idx].coords[0]; 
        double d1 = p1[1] - points[idx].coords[1]; 
        double d2 = p1[2] - points[idx].coords[2]; 
        double d3 = p1[3] - points[idx].coords[3]; 
        double d4 = p1[4] - points[idx].coords[4]; 
        double d5 = p1[5] - points[idx].coords[5];

        return d0*d0 + d1*d1 + d2*d2 + d3*d3 + d4*d4 + d5*d5;
    }

    inline double kdtree_get_pt(const size_t idx, int dim) const {
        return points[idx].coords[dim];
    }

    template<class BBOX>
    bool kdtree_get_bbox(BBOX& bb) const {
        return false;
    }
};

void extract(PointCloud& cloud, Packets& packets, Rays& rays) {
    bool* mask = new bool[cloud.points.size()];
    memset(mask, false, sizeof(bool)*cloud.points.size());

    //build index
    KDTreeSingleIndexAdaptor<
        L2_Simple_Adaptor<double, PointCloud>, PointCloud, 6> 
        index(6, cloud, KDTreeSingleIndexAdaptorParams(32));
    index.buildIndex();

    //construct packets
    for(size_t idx = 0; idx < cloud.points.size(); ++idx) {
        if(mask[idx]) continue;

        //find nearest neighbors
        size_t indices[PACKET_COUNT];
        double dists[PACKET_COUNT];

        index.knnSearch(&cloud.points[idx].coords[0], PACKET_COUNT, indices, (double*)dists);

        bool used = false;
        for(size_t i = 0; i < PACKET_COUNT; ++i) {
            int idx = indices[i];

            used |= mask[idx];
        }
        if(used) continue;

        //construct packet
        Rays prays;
        for(size_t i = 0; i < PACKET_COUNT; ++i) {
            mask[indices[i]] = true;

            Vector3 p1(cloud.points[indices[i]].coords[0], 
                       cloud.points[indices[i]].coords[1], 
                       cloud.points[indices[i]].coords[2]);
            Vector3 p2(cloud.points[indices[i]].coords[3], 
                       cloud.points[indices[i]].coords[4], 
                       cloud.points[indices[i]].coords[5]);

            prays.setFromPoints(p1, p2, cloud.points[indices[i]].id);
        }

        packets.packets.push_back(Packet(prays));
    }

    //construct rays
    for(size_t i = 0; i < cloud.points.size(); ++i) {
        if(mask[i]) continue;

        Vector3 p1(cloud.points[i].coords[0], 
                   cloud.points[i].coords[1], 
                   cloud.points[i].coords[2]);
        Vector3 p2(cloud.points[i].coords[3], 
                   cloud.points[i].coords[4], 
                   cloud.points[i].coords[5]);

        rays.setFromPoints(p1, p2, cloud.points[i].id);
    }
}
#endif

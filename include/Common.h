#pragma once

/// \file Common.h
/// \brief Contains all includes and defintions that are used by a 
/// large part of the library.


#include <limits>
#include <stdint.h>
#include <assert.h>
#include <stdio.h>
#include <vector>

#define ALIGN16 __attribute__((aligned(16)))
#define ALIGN32 __attribute__((aligned(32)))
#include "SIMD.h"

/// Simple definition for readability of infinity of double.
#define real_inf std::numeric_limits<double>::infinity()
/// Globally defined epsilon value to be used where required.
#define bepsilon 1e-6

/// The SAH cost of a traveral step in the SAH BVH tree.
#define C_TRAV 50.0
/// The SAH cost of a leaf in the SAH BVH tree.
#define C_LEAF 10.0

/// Shortcut function for fmin.
#define MIN(a,b) ((a<b)?a:b)
/// Shortcut function for fmax.
#define MAX(a,b) ((a>b)?a:b)

#pragma once

#include <cmath>
#include <cstdlib>

#include "Common.h"

struct Vector3 {
private:
    double _x, _y, _z;

public:
    inline double x() const {
        return _x;
    }
    
    inline double y() const {
        return _y;
    }
    
    inline double z() const {
        return _z;
    }

    inline double& x() {
        return _x;
    }
    
    inline double& y() {
        return _y;
    }
    
    inline double& z() {
        return _z;
    }

    inline double operator[](size_t idx) const {
        return idx==0?x():(idx==1?y():z());
    }

    inline Vector3() 
    : _x(0.0), _y(0.0), _z(0.0) {}

    inline Vector3(double cx, double cy, double cz) 
    : _x(cx), _y(cy), _z(cz) {}
    
    inline Vector3(double c) 
    : _x(c), _y(c), _z(c) {}
    
    inline Vector3(const Vector3& v) 
    : _x(v.x()), _y(v.y()), _z(v.z()) {}

    uint32_t longestAxis() {
        if((x() > y()) && (x() > z())) return 0;
        else return (y() > z()) ? 1 : 2;
    }
    
    inline void operator+=(const Vector3& v) {
        x() += v.x(); y() += v.y(); z() += v.z();
    }

    inline void operator-=(const Vector3& v) {
        x() -= v.x(); y() -= v.y(); z() -= v.z();
    }

    inline void operator*=(double c) {
        x() *= c; y() *= c; z() *= c;
    }

    inline Vector3 operator*(double c) const {
        return Vector3(x()*c, y()*c, z()*c);
    }

    inline Vector3 operator-(const Vector3& v) const {
        return Vector3(x() - v.x(), y() - v.y(), z() - v.z());
    }

    inline void operator/=(double c) {
        x() /= c; y() /= c; z() /= c;
    }
    
    inline double dot(const Vector3& v) const {
        return (x() * v.x()) 
             + (y() * v.y()) 
             + (z() * v.z());
    }

    inline Vector3 cross(const Vector3& v) const {
        return Vector3(y()*v.z() - z()*v.y(),
                       z()*v.x() - x()*v.z(),
                       x()*v.y() - y()*v.x());
    }

    inline double length2() const {
        return (*this).dot(*this);
    }

    inline double length() const {
        return sqrt(length2());
    }

    inline void comMul(const Vector3& v) {
        x() *= v.x();
        y() *= v.y();
        z() *= v.z();
    }

    void normalize() {
        double l = length();
        //XXX could avoid by addition epsilon
        if(l > 0.0) (*this) /= l; //else ignore
    }

    Vector3 normalized() const {
        Vector3 t(*this); 
        t.normalize();
        return t;
    }

    inline double minelm() const {
        return MIN(x(), MIN(y(), z()));
    }

    inline double maxelm() const {
        return MAX(x(), MAX(y(), z()));
    }

    inline Vector3 minpel(const Vector3& v) const {
        return Vector3(MIN(x(), v.x()),
                       MIN(y(), v.y()), 
                       MIN(z(), v.z()) );
    }

    inline Vector3 maxpel(const Vector3& v) const {
        return Vector3(MAX(x(), v.x()),
                       MAX(y(), v.y()), 
                       MAX(z(), v.z()) );
    }

    inline void min(const Vector3& v) {
        x() = MIN(x(), v.x()); 
        y() = MIN(y(), v.y()); 
        z() = MIN(z(), v.z());
    }

    inline void max(const Vector3& v) {
        x() = MAX(x(), v.x()); 
        y() = MAX(y(), v.y()); 
        z() = MAX(z(), v.z());
    }

    inline void set(double c) {
        x() = c;
        y() = c;
        z() = c;
    }

    inline void set(double cx, double cy, double cz) {
        x() = cx;
        y() = cy;
        z() = cz;
    }

    void set(const Vector3& v) {
        x() = v.x();
        y() = v.y();
        z() = v.z();
    }

    Vector3 inverse() const {
        return Vector3( 1.0/x(),
                        1.0/y(),
                        1.0/z());
    }
};

inline double randreal() {
    return (double)rand()/(double)RAND_MAX;
}

inline Vector3 randVector3() {
    return Vector3(randreal(), randreal(), randreal());
}

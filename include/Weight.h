#pragma once
/// \file Weight.h
/// \brief Includes all look up table related functions and data.
///
/// This file contains the prepared look up table of the integral
/// of the Gadget2 SPH Kernel and functions to calculate the weight
/// contributed by a sphere which is intersected by a ray.

#include "Common.h"
#include "SIMD.h"
#include "Packet.h"
#include "Ray.h"

const double nb2s[32] = {
    0.000000e+00, 3.225806e-02, 6.451613e-02, 9.677419e-02, 
    1.290323e-01, 1.612903e-01, 1.935484e-01, 2.258065e-01, 
    2.580645e-01, 2.903226e-01, 3.225806e-01, 3.548387e-01, 
    3.870968e-01, 4.193548e-01, 4.516129e-01, 4.838710e-01, 
    5.161290e-01, 5.483871e-01, 5.806452e-01, 6.129032e-01,
    6.451613e-01, 6.774194e-01, 7.096774e-01, 7.419355e-01, 
    7.741935e-01, 8.064516e-01, 8.387097e-01, 8.709677e-01, 
    9.032258e-01, 9.354839e-01, 9.677419e-01, 1.000000e+00
};

const double weights[32] = {
    1.909859e+00, 1.600766e+00, 1.339903e+00, 1.117350e+00, 
    9.275493e-01, 7.663731e-01, 6.304116e-01, 5.166406e-01, 
    4.221767e-01, 3.439549e-01, 2.790790e-01, 2.252531e-01, 
    1.806450e-01, 1.437670e-01, 1.133935e-01, 8.850334e-02, 
    6.823756e-02, 5.186793e-02, 3.877310e-02, 2.842040e-02,
    2.035091e-02, 1.416897e-02, 9.532368e-03, 6.145246e-03, 
    3.752101e-03, 2.132953e-03, 1.099415e-03, 4.915502e-04, 
    1.754424e-04, 4.148719e-05, 3.586255e-06, 0.000000e-06
};

const double dwdnb[32] = {
  -10.000000e+00, -8.146995e+00, -7.788923e+00, -7.961682e+00,
   -6.894688e+00, -5.686199e+00, -4.592694e+00, -4.053309e+00,
   -3.202645e+00, -2.775872e+00, -2.078016e+00, -1.807600e+00,
   -1.563011e+00, -1.342614e+00, -8.044812e-01, -7.680707e-01,
   -5.709307e-01, -5.220093e-01, -4.099975e-01, -3.436566e-01,
   -2.518071e-01, -2.033291e-01, -1.471496e-01, -1.022365e-01,
   -5.075906e-02, -4.223669e-02, -2.521322e-02, -1.555979e-02,
   -7.129928e-03, -2.409808e-03, -3.333053e-04,  0.000000e+00
};

/// \brief Lookup table for impact parameter values. 
///
/// This is mostly for understandablility of the code. The table is
/// never actually used, because it was optimized out by using a 
/// uniform difference across the LUT.

/// \brief The lookup table values for the weights.
///
/// The look up table contains the kernel intergrals calculated
/// from the Gadget2 SPH Kernel using MATLAB. The values are
/// interpolated by an optimized linear interpolator.

/// Number of elements in the look up table minus 1. Used for 
/// optimization of the interpolation search.
const double range = 31.0 - bepsilon;
/// The LUT is prepared to have equal distance between samples of
/// the function, therefore a single inverse is used to scale the
/// interpolant instead of a memory access into the nbs array.
const double df = 3.225806e-02; 
const double invdf = 31.0; 

void kernelnb2(const double, double&);
double kernelnb2(const double);

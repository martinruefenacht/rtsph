#pragma once
/// \file Parser.h
/// \brief Declares the data parsing interface for SPH data.

#include <string>
#include <vector>

#include "Common.h"
#include "Spheres.h"
#include "Ray.h"

/// \brief Parses the Gadget2 output file.
bool parse(const std::string&, Spheres& spheres);

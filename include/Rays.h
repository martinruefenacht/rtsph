#pragma once

#include <vector>

#include "Vector3.h"
#include "Ray.h"
#include "nanoflann.hpp"

//SOA
struct Rays {
    /// Origins of ray set.
    ///
    ///
    std::vector<double> ox, oy, oz;

    /// Directions of ray set.
    ///
    ///
    std::vector<double> dx, dy, dz;
    std::vector<double> ivdx, ivdy, ivdz;

    /// tmin and tmax values of ray set
    ///
    ///
    //std::vector<double> tmin, tmax;
    std::vector<double> tmax;

    ///
    ///
    ///
    std::vector<uint64_t> id;

    /// \brief Size of ray set.
    ///
    ///
    uint64_t count() const {
        return id.size();
    }

    /// \brief Translate a single ray from SOA to AOS.
    ///
    /// Extracts a single ray as a struct of Ray.
    void get(size_t idx, Ray& ray) const {
        ray.origin.x() = ox[idx];
        ray.origin.y() = oy[idx];
        ray.origin.z() = oz[idx];

        ray.direction.x() = dx[idx];
        ray.direction.y() = dy[idx];
        ray.direction.z() = dz[idx];

        ray.invdir.x() = ivdx[idx];
        ray.invdir.y() = ivdy[idx];
        ray.invdir.z() = ivdz[idx];

        //ray.tmin = tmin[idx];
        ray.tmax = tmax[idx];
        ray.id = id[idx];
    }

    /// \brief Append ray between points.
    ///
    /// Using two points calculate ray and append at end of the 
    /// rays list.
    void setFromPoints(const Vector3& p1, const Vector3& p2, uint64_t rid) {
        ox.push_back(p1.x());
        oy.push_back(p1.y());
        oz.push_back(p1.z());
        //tmin.push_back(0.0);
        
        Vector3 d; d -= p1; d += p2;
        tmax.push_back(d.length());
        d.normalize();
        dx.push_back(d.x());
        dy.push_back(d.y());
        dz.push_back(d.z());

        ivdx.push_back(1.0/d.x());
        ivdy.push_back(1.0/d.y());
        ivdz.push_back(1.0/d.z());

        id.push_back(rid);
    }

    ///
    ///
    ///
    //void parameter(size_t idx, double t, Vector3& v) {
    //    v.x() = dx[idx];
    //    v.y() = dy[idx];
    //   v.z() = dz[idx];
    //    
    //    v *= t;
    //
    //    v.x() += ox[idx];
    //    v.y() += oy[idx];
    //   v.z() += oz[idx];
    //}
};

#pragma once
/// \file Ray.h
/// \brief Ray & RayW containers

#include "Common.h"
#include "Vector3.h"

///
///
///
struct Ray {
    ///
    Vector3 origin;

    ///
    Vector3 direction;

    ///
    Vector3 invdir;

    ///
    double tmax;

    ///
    uint64_t id;
};


///
///
///
inline void points2ray(const Vector3& t0, const Vector3& t1, Ray& ray) {
    Vector3 d;
    d += t1;
    d -= t0;
    
    ray.origin = t0;

    ray.tmax = d.length();

    d.normalize();
    ray.direction = d;
    ray.invdir = d.inverse();
}

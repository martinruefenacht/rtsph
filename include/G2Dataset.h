#pragma once

#include "Spheres.h"

struct G2Dataset {
    //header
    uint32_t npart[6];
    double hmass[6];
    double time;
    double redshift;
    uint32_t sft;
    uint32_t feedback;
    uint32_t npartTotal[6];
    uint32_t flag_cooling;
    uint32_t num_files;
    double BoxSize;
    double Omega0;
    double OmegaLambda;
    double HubbleParam;
    uint32_t flag_stellarage;
    uint32_t flag_metals;
    uint32_t hashtabsize;

    //data
    float *r, *v, *mass, *u, *rho, *h;
    uint32_t *id;

    void getSpheres(Spheres&) const;
};

void parseG2Dataset(std::string&, G2Dataset&);

#! /usr/bin/python

import numpy as np
from struct import unpack
import matplotlib.pyplot as plt
import matplotlib.mlab as ml
from matplotlib.colors import LogNorm

import parse

#hdr, pos, rad = parse.dataset("aux/data/Data_025")
#ngas = hdr['npart'][0]

#tmp = zip(*pos)

#print len(tmp[0])

#tx = np.array(tmp[0])
#ty = np.array(tmp[1])
#tz = np.array(tmp[2])

fid = open("inim.cds","rb")
cou = unpack("L",fid.read(8))[0]
print cou
tw = np.fromfile(fid,dtype="double",count=cou)

w = np.reshape(tw,(1000,1000))

print "max ", tw.max()
print "min ", tw.min()

fid.close()

plt.imshow(w,cmap='jet')

#plt.contour(xi, yi, zi, 15, linewidths=0.5, colors='k')
#plt.pcolormesh(xi,yi,wi,cmap='jet',norm=LogNorm())

plt.colorbar()
#plt.scatter(x,y,marker='o',c='b',s=5,zorder=10)
#plt.xlim(xmin,xmax)
#plt.ylim(ymin,ymax)
plt.show()
#plt.savefig("rad.png",format="png")

#! /usr/bin/python2

from math import log
from struct import unpack

dbin = open('image.cds','rb')
pout = open('image.pnm','w')

pout.write("P3\n")
pout.write("500 500\n")
pout.write("255\n")

size = unpack('L', dbin.read(8))
print size[0]

minval = -5.4
maxval =  0.4
scale = 255.0 / (maxval - minval)

actmin =  100.0
actmax = -100.0

for iy in range(0, 500):
    for ix in range(0,500):
        val = log(unpack('d', dbin.read(8))[0])

        if val < actmin: 
            actmin = val
        if val > actmax:
            actmax = val

        p = str(int((val - minval)*scale))
        pixel = "" + p + " " + p + " " + p + " "

        pout.write(pixel)
    pout.write("\n")

dbin.close()
pout.close()

print actmin, actmax

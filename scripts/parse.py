#! /usr/bin/python2

#Python imports
from struct import unpack

#Load dataset header
def loadHeadSection(binary):
    header = {}

    #Header blocksize
    #blocksize = unpack("I",binary.read(4))
    binary.seek(4,1)

    #Read header fields
    header['npart']           = unpack("IIIIII",binary.read(24))
    header['mass']            = unpack("dddddd",binary.read(48))
    header['time']            = unpack("d",binary.read(8))
    header['redshift']        = unpack("d",binary.read(8))
    header['sfr']             = unpack("I",binary.read(4))
    header['feedback']        = unpack("I",binary.read(4))
    header['npartTotal']      = unpack("IIIIII",binary.read(24))
    header['flag_cooling']    = unpack("I",binary.read(4))
    header['num_files']       = unpack("I",binary.read(4))
    header['BoxSize']         = unpack("d",binary.read(8))
    header['Omega0']          = unpack("d",binary.read(8))
    header['OmegaLambda']     = unpack("d",binary.read(8))
    header['HubbleParam']     = unpack("d",binary.read(8))
    header['flag_stellarage'] = unpack("I",binary.read(4))
    header['flag_metals']     = unpack("I",binary.read(4))
    header['hashtabsize']     = unpack("I",binary.read(4))

    #Read remaining of block
#    count = 176
#    bytesleft = 256 - count*4
#    binary.read(bytesleft)
    binary.seek(256-176,1)
    #Read another blocksize?
#    blocksize = unpack("I",binary.read(4))
    binary.seek(8,1)

    #Return header data
    return header

#Load dataset R section
def loadRSection(binary,header):
    rs = []

    #R blocksize
    #blocksize = unpack("I",binary.read(4))
    binary.seek(4,1)
    
    #Read sum(npart) floats as (x,y,z)
    gas = header['npart'][0]
    for i in range(gas):
        d = unpack("fff",binary.read(12))
        rs.append(d)

    rest = sum(header['npart']) - gas
    binary.seek(12*rest)

    #Read another blocksize?
    #blocksize = unpack("I",binary.read(4))
    binary.seek(4,1)

    #Return position data
    return rs

#Load dataset V section
def loadVSection(binary,header):
    #Fake reading velocity section
    count = 4 + sum(header['npart'])*3*4 + 4
    binary.seek(count,1)

#Load dataset I section
def loadISection(binary,header):
    #Fake reading id section
    count = 4 + sum(header['npart'])*4 + 4
    binary.seek(count,1)

#Load dataset M section
def loadMSection(binary,header):
    masses = []
    
    #Count parts
    parts = 0
    for part in range(len(header['npart'])):
        if (header['npart'][part] > 0) and (header['mass'][part] == 0.0):
            parts += header['npart'][part]

    if not parts > 0:
        return masses

    #M blocksize
    #blocksize = unpack("I",binary.read(4))
    binary.seek(4,1)
    
    #Read n_gas floats
    #for i in xrange(parts):
    #   masses.append(unpack("f",binary.read(4)))
    binary.seek(4*parts,1)

    #Read another blocksize?
    #blocksize = unpack("I",binary.read(4))
    binary.seek(4,1)

    #Return position data
    return masses

#Load dataset T section
def loadTSection(binary,header):
    thermals = []

    #Check if file contains gas thermal energy
    if not header['npart'][0] > 0:
        return thermals

    #r blocksize
    #blocksize = unpack("i",binary.read(4))
    binary.seek(4,1)
    
    #read n_gas floats
    #for i in xrange(header['npart'][0]):
    #   thermals.append(unpack("f",binary.read(4)))
    binary.seek(4*header['npart'][0],1)

    #read another blocksize?
    #blocksize = unpack("i",binary.read(4))
    binary.seek(4,1)

    #return position data
    return thermals

#Load dataset D section
def loadDSection(binary,header):
    densities = []

    #Check if file contains gas thermal energy
    if not header['npart'][0] > 0:
        return densities

    #R blocksize
    #blocksize = unpack("I",binary.read(4))
    binary.seek(4,1)
    
    #Read n_gas floats
    #    for i in xrange(header['npart'][0]):
    #    densities.append(unpack("f",binary.read(4)))
    binary.seek(4*header['npart'][0],1)

    #Read another blocksize?
    #blocksize = unpack("I",binary.read(4))
    binary.seek(4,1)

    #Return position data
    return densities

#Load dataset H Section
def loadHSection(binary,header):
    radii = []

    #Check if file contains gas thermal energy
    if not header['npart'][0] > 0:
        return radii

    #R blocksize
    #blocksize = unpack("I",binary.read(4))
    binary.seek(4,1)
    
    #Read n_gas floats
    for i in range(header['npart'][0]):
        d = binary.read(4)
        radii.append(unpack("f",d)[0])

    #Read another blocksize?
    #blocksize = unpack("I",binary.read(4))
    binary.seek(4,1)

    #Return position data
    return radii

#Parse dataset
def dataset(filename):
    #Open binary dataset
    binary = open(filename,'rb')

    #Load dataset parts
    header = loadHeadSection(binary)
    positions = loadRSection(binary,header)
    loadVSection(binary,header)
    loadISection(binary,header)
    loadMSection(binary,header)
    loadTSection(binary,header)
    loadDSection(binary,header)
    radii = loadHSection(binary,header)

    #Close binary dataset
    binary.close()

    #Return sphere data
    return header, positions, radii

if __name__ == "__main__":
    h, p, r = dataset("aux/data/Data_025")
    print p[5000]

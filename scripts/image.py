#! /usr/bin/python2

from struct import pack

bout = open('image.lns', 'wb')

count = 500*500

#write header
bout.write(pack('L', count))

#write data
#p1x, p1y, p1z, p2x, p2y, p2z, ... * count
for iy in range(0, 500):
    for ix in range(0, 500):
        px = (float(ix) + 0.5) / 500.0; 
        py = (float(iy) + 0.5) / 500.0;

        x1 = px * 10000.0
        y1 = py * 10000.0
        z1 = -1000.0

        x2 = px * 10000.0
        y2 = py * 10000.0
        z2 = 11000.0

        bout.write(pack('dddddd', x1, y1, z1, x2, y2, z2))

bout.close()

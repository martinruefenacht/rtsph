#! /usr/bin/python2

import numpy as np
from struct import unpack
import matplotlib.pyplot as plt
import matplotlib.mlab as ml
from matplotlib.colors import LogNorm
from mpl_toolkits.mplot3d import Axes3D

fid = open("im.cds","rb")

nx = unpack("L", fid.read(8))[0]
ny = unpack("L", fid.read(8))[0]

print nx, ny

tn = np.fromfile(fid, dtype='double', count=nx*ny)

ns = np.reshape(tn, (ny,nx))
print "total ", np.sum(ns)

fid.close()
print "read"

#ns = np.rot90(ns,3)
#ns = ns[::,::-1]
plt.imshow(ns, norm=LogNorm(), cmap='jet')
#plt.imshow(ns, cmap='jet')

#plt.imshow(ns, cmap='jet', interpolation='nearest')
#plt.imshow(ns, cmap='jet')

#plt.contour(xi, yi, zi, 15, linewidths=0.5, colors='k')
#plt.pcolormesh(xi,yi,wi,cmap='jet',norm=LogNorm())

plt.xlabel("X Axis of simulation (10 KPc)")
plt.ylabel("Y Axis of simulation (10 KPc)")
plt.title("Column Density of projection along Z Axis")


plt.rc('text',usetex=True)
cbar = plt.colorbar()
cbar.ax.set_ylabel("Column Density (\(cm^{-2}\))")
#plt.scatter(x,y,marker='o',c='b',s=5,zorder=10)
#plt.xlim(xmin,xmax)
#plt.ylim(ymin,ymax)
#plt.show()
plt.savefig("im.png",format="png")

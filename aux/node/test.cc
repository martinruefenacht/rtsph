#include <cstdint>
#include <iostream>

uint32_t idxmask = 0x07ffffff;
uint32_t runmask = 0xf8000000;

void  encode(uint32_t min, uint32_t max, uint32_t& enc) {
    uint32_t run = max-min+1;

    enc = 0;
    enc |= min & idxmask;
    enc |= ((run << 27)&runmask);
}

void decode(uint32_t enc, uint32_t& min, uint32_t& max) {
    min = enc & idxmask;
    uint32_t run = (enc & runmask) >> 27;

    max = run+min-1;
}

int main() {
    uint32_t min = 134217727; //<0x08000000 
    uint32_t max = 134217735;

    std::cout << std::hex << min+1 << std::endl;

    min = 0x07ffffff;

    std::cout << std::hex << min << std::endl;
    
    uint32_t enc;
    encode(min,max,enc);
    std::cout << std::hex << enc << std::endl;
    uint32_t dmin, dmax;
    decode(enc,dmin,dmax);
    std::cout << std::dec << dmin << " " << dmax << std::endl;
}

function weights = hspline(r,h)
	u = r ./ h;

	weight = zeros(length(r));
	for i=u
		if( (0 <= i) && (i <= 1.0) )
			weight = 4.0 - 6.0*u.^2 + 3.0*u.^3; 
		else if( (1.0 < u) && (u <= 2.0) )
			weight = (2.0-u).^3;
		else
			weight = 0;
		end
	end

	weights = weight ./ (4.0*pi);
end

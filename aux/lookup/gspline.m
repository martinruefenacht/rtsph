function weights = gspline(r,h)
	u = r ./ h;

	weight = zeros(length(r));
	for i=u
		if( (0 <= i) && (i <= 0.5) )
			weight = 1 - 6*i.^2 + 6*i.^3; 
		else if( (0.5 < i) && (i <= 1) )
			weight = 2*(1-i).^3;
		else
			weight = 0;
		end
	end

	weights = weight .* 8 ./ (pi*h^3);

	%u = r ./ h;
	%weight = NaN(size(u));
	%weight((0.0<=u)&&(u<=0.5)) = 1 - 6*u((0.0<=u)&&(u<=0.5))^2 + 6*u((0.0<=u)&&(u<=0.5))^3;
	%weight((0.5< u)&&(u<=1.0)) = 2*(1-u(0.5< u)&&(u<=1.0))^3;
	%weight(1.0<u) = 0.0;

	%weights = weight .* 8.0 ./ (pi*h^3);
end

function weights = cubspline(r,h)
	u = r ./ h;

	weight = zeros(length(r));
	for i=u
		if( (0 <= i) && (i <= 0.5) )
			weight = 1 - 6*u.^2 + 6*u.^3; 
		else if( (0.5 < u) && (u <= 1) )
			weight = 2*(1-u).^3;
		else
			weight = 0;
		end
	end

	weights = weight .* 8 ./ (pi*h^3);
end

% ReadGadgetField: Read a single field from a Gadget file
%
% D=ReadGadgetField(fname,Field)
%
% ARGUMENTS
%  Input
%   fname   The name of the file to read
%   Field   The name of the field to read
%            One of
%             hdr       Header
%             r         Particle positions
%             v         Particle velocities
%             id        Particle IDs
%             mass      Particle masses (if available)
%             u         Gas internal energies (if available)
%             rho       Gas densities (if available)
%             h         Smoothing length (if available)
%
%  Output
%   D       The data for that field
%
%  Elements of hdr:
%   npart
%   mass
%   time
%   redshift
%   sft
%   feedback
%   npartTotal
%
% SEE ALSO
%  readgadget ReadGadgetHeader

% AUTHOR: Eric Tittley
%
% COMPATIBILITY: Matlab Octave
%
% HISTORY
%  11 05 02 First version, based on readgadget
%  11 06 24 Delete D when re-reading.
%  13 02 13 Use the function ReadGadgetHeader to read the header.

function D=ReadGadgetField(fname,Field)

% Open the file
fid=fopen(fname,'rb');
if(fid<=0)
 disp(['ERROR: readgadget: unable to open file: ',fname])
 return
end

% Read the header
hdr=ReadGadgetHeader(fid);

% If the header is all we need, then close file and return
if(strcmp(Field,'hdr'))
 D=hdr;
 fclose(fid);
 return
end

% The base data, position, velocity, and particle type
N=sum(hdr.npart);

blocksize=fread(fid,1,'uint');
D = fread(fid,[3,N],'float');
blocksize=fread(fid,1,'uint');

if(strcmp(Field,'r'))
 fclose(fid);
 return
end

clear D
blocksize=fread(fid,1,'uint');
D = fread(fid,[3,N],'float');
blocksize=fread(fid,1,'uint');

if(strcmp(Field,'v'))
 fclose(fid);
 return
end

clear D
blocksize=fread(fid,1,'uint');
D = fread(fid,N,'uint');
blocksize=fread(fid,1,'uint');

if(strcmp(Field,'id'))
 fclose(fid);
 return
end

% The mass, if necessary
clear D
ind=find((hdr.npart > 0) & (hdr.mass == 0));
if(~isempty(ind))
 Nwithmass=sum(hdr.npart(ind));
 blocksize=fread(fid,1,'uint');
 D.mass = fread(fid,Nwithmass,'float');
 blocksize=fread(fid,1,'uint');
end
if(strcmp(Field,'mass'))
 if(isempty(ind))
  fclose(fid);
  error('ERROR: ReadGadgetField: Field "mass" not available')
 end
 fclose(fid);
 return
end

% For the gas, read the thermal energy and the density
Ngas=hdr.npart(1);
if(Ngas > 0)

 clear D
 blocksize=fread(fid,1,'uint');
 D = fread(fid,Ngas,'float');
 blocksize=fread(fid,1,'uint');
 if(strcmp(Field,'u'))
  fclose(fid);
  return
 end

 clear D
 blocksize=fread(fid,1,'uint');
 D = fread(fid,Ngas,'float');
 blocksize=fread(fid,1,'uint');
 if(strcmp(Field,'rho'))
  fclose(fid);
  return
 end

 clear D
 blocksize=fread(fid,1,'uint');
 D = fread(fid,Ngas,'float');
 blocksize=fread(fid,1,'uint');
 if(strcmp(Field,'h'))
  fclose(fid);
  return
 end

else
 if( strcmp(Field,'u') || strcmp(Field,'rho') || strcmp(Field,'h') )
  error('ERROR: ReadGadgetField: Gas fields not available')
 end
end

% Close the file
fclose(fid);

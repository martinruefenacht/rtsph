#include "Parser.h"
#include "BVH.h"

#include <omp.h>
#include <iostream>
#include <stack>

bool verify(const AABB& parent, const AABB& child) {
    //X min
    if(parent.min.x > child.min.x)
        return false;

    //X max
    if(parent.max.x < child.max.x)
        return false;

    //Y min
    if(parent.min.y > child.min.y)
        return false;

    //Y max
    if(parent.max.y < child.max.y)
        return false;

    //Z min
    if(parent.min.z > child.min.z)
        return false;

    //Z max
    if(parent.max.z < child.max.z)
        return false;

    return true;
}

bool verify(const AABB& parent, const AABB& c1, const AABB& c2 ) {
    return verify(parent,c1) && verify(parent,c2);
}

bool verify(const AABB& parent, std::vector<Sphere>& sphs, uint32_t min, uint32_t max ) {
    for(uint32_t i = min; i <= max; ++i) {
        AABB lbbox;
        const Sphere& sph = sphs[i];

        Vector3 r3(sph.radius,sph.radius,sph.radius);
        lbbox.min = sph.center - r3;
        lbbox.max = sph.center + r3;

        if(!verify(parent,lbbox))
            return false;
    }

    return true;
} 

int main(int argc, char** argv) {
    spheres_t spheres;
    parse("data/Data_025",spheres);
    CBVH tree;

    std::cout << "------debug------" << std::endl;
    std::cout << "CBVHNode " << sizeof(CBVHNode) << std::endl;

    
    //Timing
    double start, end;

    /*Sphere s0; s0.center = Vector3(-5, 0, 0); s0.radius = 1;
    Sphere s1; s1.center = Vector3( 2, 0, 0); s1.radius = 1;
    Sphere s2; s2.center = Vector3(-6, 0, 0); s2.radius = 1;
    Sphere s3; s3.center = Vector3(-7, 0, 0); s3.radius = 1;
    Sphere s4; s4.center = Vector3( 6, 0, 0); s4.radius = 1;
    Sphere s5; s5.center = Vector3( 8, 0, 0); s5.radius = 1;
    Sphere s6; s6.center = Vector3( 1, 0, 0); s6.radius = 1;
    Sphere s7; s7.center = Vector3( 4, 0, 0); s7.radius = 1;
    Sphere s8; s8.center = Vector3(-1, 0, 0); s8.radius = 1;
    Sphere s9; s9.center = Vector3( 5, 0, 0); s9.radius = 1;
    spheres.push_back(s0);
    spheres.push_back(s1);
    spheres.push_back(s2);
    spheres.push_back(s3);
    spheres.push_back(s4);
    spheres.push_back(s5);
    spheres.push_back(s6);
    spheres.push_back(s7);
    spheres.push_back(s8);
    spheres.push_back(s9);*/

    //Construction
    tree.construct(&spheres);
    tree.print();

    //check if children aabb are in parent aabb
    /*std::stack<uint32_t> stack;
    stack.push(0);

    while(!stack.empty()) {
        uint32_t inode = stack.top();
        stack.pop();

        const CBVHNode& node = tree.nodes[inode];

        if(node.leaf) {
            for(uint32_t i=node.idxmin;i<=node.idxmax;++i) {
                const Sphere& sph = spheres[tree.idxs[i]];

                //build aabb
                AABB saabb;
                Vector3 r3(sph.radius,sph.radius,sph.radius);
                Vector3 min = sph.center - r3;
                Vector3 max = sph.center + r3;
                saabb.min = min;
                saabb.max = max;

                if(!verify(node.bbox,saabb)) {
                    std::cerr << "ERR saabb not in bbox" << std::endl;
                    break;
                }
            }

        }
        else {
            const CBVHNode& rNode = tree.nodes[node.rightIdx];
            const CBVHNode& lNode = tree.nodes[node.leftIdx];

            //Check AABB
            if(!verify(node.bbox,rNode.bbox,lNode.bbox)){
                std::cerr << "ERR does not contain node aabb" << std::endl;
            }

            stack.push(node.rightIdx);
            stack.push(node.leftIdx);
        }
    }
    std::cout << "Test passed" << std::endl;*/
}
